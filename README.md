**If you read this, then you should know this:** I have started this project to learn about vertx, so do not expect much from it.

About
-----------
Allows you to use OpenCV face detection on various video sources.

Video sources are defined through REST API -> there is simple WEB GUI to operate these services.

Supported video sources are: webcameras, H.264 unsecured video streams or files using full path.

Video stream for each resource is available as MJEPG (for WEB GUI), Transport Stream protocol. 


How to compile
-----------
Run _mvn package_ then you can grab final fat jar from /target directory.

I did not bother to filter JavaCV dependencies (yet) to specific platforms and used libraries. As a result it leads to huge fat jar (200MB>), since it brings compiled libraries for all supported platforms. 

How to run
-----------

JAR

Run _java -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory -jar filename.jar run cz.klitea.recognitor.rxjava.verticle.WebServiceVerticle -conf conf/default.json_.

Don't forget o change jar name to real name and correct config file path.

-----------

IDE

1. Set main class _cz.klitea.recognitor.RecognitorLauncher_
2. Program arguments _run cz.klitea.recognitor.rxjava.verticle.WebServiceVerticle -conf conf/default.json_
3. VM options _-Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory_

Some classes are generated at compile time, so don't panic when your IDE reports missing classes.

---
