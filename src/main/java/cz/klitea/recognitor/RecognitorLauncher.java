package cz.klitea.recognitor;

import io.vertx.core.Launcher;

/**
 * I added this only because some IDEs (NetBeans) has problem
 * with using Launcher from dependency package.
 *
 * @author fcejka
 */
public class RecognitorLauncher extends Launcher
{
}
