package cz.klitea.recognitor.entity;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import org.jetbrains.annotations.NotNull;

/**
 * Well, sometimes you need your data structured and dynamic JsonObject is not enough.
 *
 * @author fcejka
 */
@DataObject(generateConverter = true)
public class VideoStreamClientEntity
{
	public VideoStreamClientEntity fromString(String json)
	{
		return new VideoStreamClientEntity(new JsonObject(json));
	}

	private VideoStreamClientId id;
	private String address;
	private String connectedAt;

	public VideoStreamClientEntity()
	{
	}

	public VideoStreamClientEntity(String address, String connectedAt)
	{
		this.address = address;
		this.connectedAt = connectedAt;
	}

	public VideoStreamClientEntity(JsonObject json)
	{
		cz.klitea.recognitor.entity.VideoStreamClientEntityConverter.fromJson(json, this);
		// Lazy, so workaround.
		if (json.containsKey("id")) this.id = new VideoStreamClientId(json.getString("id"));
	}

	@NotNull
	public VideoStreamClientEntity(VideoStreamClientId id, String address, String connectedAt)
	{
		this.id = id;
		this.address = address;
		this.connectedAt = connectedAt;
	}

	public VideoStreamClientId getId()
	{
		return id;
	}

	public void setId(VideoStreamClientId id)
	{
		this.id = id;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getConnectedAt()
	{
		return connectedAt;
	}

	public void setConnectedAt(String connectedAt)
	{
		this.connectedAt = connectedAt;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof VideoStreamClientEntity)) return false;

		VideoStreamClientEntity that = (VideoStreamClientEntity) o;

		if (id != null ? !id.equals(that.id) : that.id != null) return false;
		if (address != null ? !address.equals(that.address) : that.address != null) return false;
		return connectedAt != null ? connectedAt.equals(that.connectedAt) : that.connectedAt == null;
	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (address != null ? address.hashCode() : 0);
		result = 31 * result + (connectedAt != null ? connectedAt.hashCode() : 0);
		return result;
	}

	public JsonObject toJson()
	{
		JsonObject json = new JsonObject();
		cz.klitea.recognitor.entity.VideoStreamClientEntityConverter.toJson(this, json);
		// little workaround, since I want to show ID as value, not object
		if (id != null) json.put("id", id.toString());
		return json;
	}

	public String toString()
	{
		return toJson().encode();
	}
}
