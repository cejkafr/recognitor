package cz.klitea.recognitor.entity;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

/**
 * Well, sometimes you need your data structured and dynamic JsonObject is not enough.
 *
 * @author fcejka
 */
@DataObject(generateConverter = true)
public class VideoStreamEntity
{
	public static VideoStreamEntity fromString(String json)
	{
		return new VideoStreamEntity(new JsonObject(json));
	}

	private VideoStreamId id;
	private String name;
	private String type;
	private String uri;
	private boolean streamEnabled;
	private int streamPort;

	public VideoStreamEntity()
	{
	}

	public VideoStreamEntity(JsonObject json)
	{
		cz.klitea.recognitor.entity.VideoStreamEntityConverter.fromJson(json, this);
		// Lazy, so workaround.
		if (json.containsKey("id")) this.id = new VideoStreamId(json.getString("id"));
	}

	public VideoStreamEntity(String name, String type, String uri, boolean streamEnabled, int streamPort)
	{
		this.name = name;
		this.type = type;
		this.uri = uri;
		this.streamEnabled = streamEnabled;
		this.streamPort = streamPort;
	}

	public VideoStreamEntity(VideoStreamId id, String name, String type, String uri, boolean streamEnabled, int streamPort)
	{
		this.id = id;
		this.name = name;
		this.type = type;
		this.uri = uri;
		this.streamEnabled = streamEnabled;
		this.streamPort = streamPort;
	}

	public VideoStreamId getId()
	{
		return id;
	}

	public void setId(VideoStreamId id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getUri()
	{
		return uri;
	}

	public void setUri(String uri)
	{
		this.uri = uri;
	}

	public boolean isStreamEnabled()
	{
		return streamEnabled;
	}

	public void setStreamEnabled(boolean streamEnabled)
	{
		this.streamEnabled = streamEnabled;
	}

	public int getStreamPort()
	{
		return streamPort;
	}

	public void setStreamPort(int streamPort)
	{
		this.streamPort = streamPort;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof VideoStreamEntity)) return false;

		VideoStreamEntity that = (VideoStreamEntity) o;

		if (streamEnabled != that.streamEnabled) return false;
		if (streamPort != that.streamPort) return false;
		if (id != null ? !id.equals(that.id) : that.id != null) return false;
		if (name != null ? !name.equals(that.name) : that.name != null) return false;
		if (type != null ? !type.equals(that.type) : that.type != null) return false;
		return uri != null ? uri.equals(that.uri) : that.uri == null;
	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (type != null ? type.hashCode() : 0);
		result = 31 * result + (uri != null ? uri.hashCode() : 0);
		result = 31 * result + (streamEnabled ? 1 : 0);
		result = 31 * result + streamPort;
		return result;
	}

	public JsonObject toJson()
	{
		JsonObject json = new JsonObject();
		cz.klitea.recognitor.entity.VideoStreamEntityConverter.toJson(this, json);
		// little workaround, since I want to show ID as value, not object
		if (id != null) json.put("id", id.toString());
		return json;
	}

	public String toString()
	{
		return toJson().encode();
	}
}
