package cz.klitea.recognitor.entity;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * I like ID classes because it removes confusion when working with multiple IDs at one...at cost of complexity.
 *
 * @author fcejka
 */
@DataObject(generateConverter = false)
public final class VideoStreamId
{
	private final String value;

	public VideoStreamId()
	{
		value = UUID.randomUUID().toString();
	}

	public VideoStreamId(JsonObject json)
	{
		this.value = json.getString("value");
	}

	@NotNull
	public VideoStreamId(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof VideoStreamId)) return false;

		VideoStreamId that = (VideoStreamId) o;

		return value.equals(that.value);
	}

	@Override
	public int hashCode()
	{
		return value.hashCode();
	}

	public JsonObject toJson()
	{
		return new JsonObject().put("value", value);
	}

	public String toString()
	{
		return value;
	}
}
