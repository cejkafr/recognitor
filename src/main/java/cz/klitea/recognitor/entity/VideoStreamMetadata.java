package cz.klitea.recognitor.entity;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.core.json.JsonObject;

/**
 * Provides information about video file/stream.
 *
 * @author frantisek.cejka
 */
@DataObject(generateConverter = true)
public class VideoStreamMetadata
{
	public static class VideoStreamMetadataResponse
	{
		private VideoStreamMetadata metadata;
		private String status;

		public VideoStreamMetadataResponse(VideoStreamMetadata metadata, String status)
		{
			this.metadata = metadata;
			this.status = status;
		}

		public VideoStreamMetadata getMetadata()
		{
			return metadata;
		}

		public String getStatus()
		{
			return status;
		}
	}

	public static VideoStreamMetadata fromString(String jsonString)
	{
		return new VideoStreamMetadata(new JsonObject(jsonString));
	}

	private int imageWidth;
	private int imageHeight;
	private int frameRate;
	private int videoBitrate;
	private long lengthInSec;
	private long lengthInFrames;
	private String codec;
	private String format;
	private int pixelFormat;

	public VideoStreamMetadata()
	{
	}

	public VideoStreamMetadata(JsonObject jsonObject)
	{
		VideoStreamMetadataConverter.fromJson(jsonObject, this);
	}

	public VideoStreamMetadata(int imageWidth, int imageHeight, int frameRate, int videoBitrate,
							   long lengthInSec, long lengthInFrames, String codec, String format, int pixelFormat)
	{
		this.imageWidth = imageWidth;
		this.imageHeight = imageHeight;
		this.frameRate = frameRate;
		this.videoBitrate = videoBitrate;
		this.lengthInSec = lengthInSec;
		this.lengthInFrames = lengthInFrames;
		this.codec = codec;
		this.format = format;
		this.pixelFormat = pixelFormat;
	}

	public int getImageWidth()
	{
		return imageWidth;
	}

	@Fluent
	public VideoStreamMetadata setImageWidth(int imageWidth)
	{
		this.imageWidth = imageWidth;
		return this;
	}

	public int getImageHeight()
	{
		return imageHeight;
	}

	@Fluent
	public VideoStreamMetadata setImageHeight(int imageHeight)
	{
		this.imageHeight = imageHeight;
		return this;
	}

	public int getFrameRate()
	{
		return frameRate;
	}

	@Fluent
	public VideoStreamMetadata setFrameRate(int frameRate)
	{
		this.frameRate = frameRate;
		return this;
	}

	public int getVideoBitrate()
	{
		return videoBitrate;
	}

	@Fluent
	public VideoStreamMetadata setVideoBitrate(int videoBitrate)
	{
		this.videoBitrate = videoBitrate;
		return this;
	}

	public long getLengthInSec()
	{
		return lengthInSec;
	}

	@Fluent
	public VideoStreamMetadata setLengthInSec(long lengthInSec)
	{
		this.lengthInSec = lengthInSec;
		return this;
	}

	public long getLengthInFrames()
	{
		return lengthInFrames;
	}

	@Fluent
	public VideoStreamMetadata setLengthInFrames(long lengthInFrames)
	{
		this.lengthInFrames = lengthInFrames;
		return this;
	}

	public String getCodec()
	{
		return codec;
	}

	@Fluent
	public VideoStreamMetadata setCodec(String codec)
	{
		this.codec = codec;
		return this;
	}

	public String getFormat()
	{
		return format;
	}

	@Fluent
	public VideoStreamMetadata setFormat(String format)
	{
		this.format = format;
		return this;
	}

	public int getPixelFormat()
	{
		return pixelFormat;
	}

	@Fluent
	public VideoStreamMetadata setPixelFormat(int pixelFormat)
	{
		this.pixelFormat = pixelFormat;
		return this;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof VideoStreamMetadata)) return false;

		VideoStreamMetadata that = (VideoStreamMetadata) o;

		if (imageWidth != that.imageWidth) return false;
		if (imageHeight != that.imageHeight) return false;
		if (frameRate != that.frameRate) return false;
		if (videoBitrate != that.videoBitrate) return false;
		if (lengthInSec != that.lengthInSec) return false;
		if (lengthInFrames != that.lengthInFrames) return false;
		if (pixelFormat != that.pixelFormat) return false;
		if (codec != null ? !codec.equals(that.codec) : that.codec != null) return false;
		return format != null ? format.equals(that.format) : that.format == null;
	}

	@Override
	public int hashCode()
	{
		int result = imageWidth;
		result = 31 * result + imageHeight;
		result = 31 * result + frameRate;
		result = 31 * result + videoBitrate;
		result = 31 * result + (int) (lengthInSec ^ (lengthInSec >>> 32));
		result = 31 * result + (int) (lengthInFrames ^ (lengthInFrames >>> 32));
		result = 31 * result + (codec != null ? codec.hashCode() : 0);
		result = 31 * result + (format != null ? format.hashCode() : 0);
		result = 31 * result + pixelFormat;
		return result;
	}

	public JsonObject toJson()
	{
		JsonObject jsonObject = new JsonObject();
		VideoStreamMetadataConverter.toJson(this, jsonObject);
		return jsonObject;
	}

	@Override
	public String toString()
	{
		return toJson().encode();
	}
}
