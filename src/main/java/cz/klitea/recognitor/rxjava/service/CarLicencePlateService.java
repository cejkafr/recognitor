package cz.klitea.recognitor.rxjava.service;

import com.openalpr.jni.AlprResults;
import cz.klitea.recognitor.service.CarLicencePlateServiceImpl;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.rxjava.core.Vertx;
import rx.Single;

/**
 * This is RX wrapper for video stream service. It's manually written ATM, but I'm sure it can be generated.
 *
 * @author fcejka
 */
public class CarLicencePlateService
{
	public static CarLicencePlateService create(Vertx vertx)
	{
		return new CarLicencePlateService(new CarLicencePlateServiceImpl(vertx.getDelegate()));
	}

	private final cz.klitea.recognitor.service.CarLicencePlateService delegate;

	public CarLicencePlateService(cz.klitea.recognitor.service.CarLicencePlateService delegate)
	{
		this.delegate = delegate;
	}

	public cz.klitea.recognitor.service.CarLicencePlateService getDelegate()
	{
		return delegate;
	}

	public Single<AlprResults> detectAndRead(FrameMatrix frame)
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.detectAndRead(frame, fut);
		}));
	}
}
