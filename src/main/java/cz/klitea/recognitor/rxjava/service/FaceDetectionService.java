package cz.klitea.recognitor.rxjava.service;

import cz.klitea.recognitor.service.FaceDet;
import cz.klitea.recognitor.service.FaceDetectionConfig;
import cz.klitea.recognitor.service.FaceDetectionServiceImpl;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.rxjava.core.Vertx;
import rx.Single;

/**
 * This is RX wrapper for video stream service. It's manually written ATM, but I'm sure it can be generated.
 *
 * @author fcejka
 */
public class FaceDetectionService
{
	public static FaceDetectionService create(Vertx vertx, FaceDetectionConfig config)
	{
		return new FaceDetectionService(new FaceDetectionServiceImpl(vertx.getDelegate(), config));
	}

	private final cz.klitea.recognitor.service.FaceDetectionService delegate;

	public FaceDetectionService(cz.klitea.recognitor.service.FaceDetectionService delegate)
	{
		this.delegate = delegate;
	}

	public cz.klitea.recognitor.service.FaceDetectionService getDelegate()
	{
		return delegate;
	}

	public Single<FaceDet.List> detect(FrameMatrix frame)
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.detect(frame, fut);
		}));
	}
}
