package cz.klitea.recognitor.rxjava.service;

import cz.klitea.recognitor.entity.Area;
import cz.klitea.recognitor.service.FaceDetectionServiceImpl;
import cz.klitea.recognitor.service.MotionDetectionServiceImpl;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.rxjava.core.Vertx;
import org.bytedeco.javacpp.opencv_core;
import rx.Single;

import java.util.Set;

/**
 * This is RX wrapper for video stream service. It's manually written ATM, but I'm sure it can be generated.
 *
 * @author fcejka
 */
public class MotionDetectionService
{
	public static MotionDetectionService create(Vertx vertx, Area[] sa)
	{
		return new MotionDetectionService(new MotionDetectionServiceImpl(vertx.getDelegate(), sa));
	}

	private final cz.klitea.recognitor.service.MotionDetectionService delegate;

	public MotionDetectionService(cz.klitea.recognitor.service.MotionDetectionService delegate)
	{
		this.delegate = delegate;
	}

	public cz.klitea.recognitor.service.MotionDetectionService getDelegate()
	{
		return delegate;
	}

	public Single<Set<Integer>> detect(FrameMatrix frame)
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.detect(frame, fut);
		}));
	}
}
