package cz.klitea.recognitor.rxjava.service;

import cz.klitea.recognitor.entity.VideoStreamClientEntity;
import cz.klitea.recognitor.entity.VideoStreamClientId;
import cz.klitea.recognitor.entity.VideoStreamEntity;
import cz.klitea.recognitor.entity.VideoStreamId;
import cz.klitea.recognitor.service.VideoStreamServiceImpl;
import cz.klitea.recognitor.service.VideoStreamServiceVertxEBProxy;
import io.vertx.rxjava.core.Vertx;
import rx.Single;

import java.util.List;

/**
 * This is RX wrapper for video stream service. It's manually written ATM, but I'm sure it can be generated.
 *
 * @author fcejka
 */
public class VideoStreamService
{
	public static VideoStreamService create(Vertx vertx)
	{
		return new VideoStreamService(new VideoStreamServiceImpl(vertx.getDelegate()));
	}

	public static VideoStreamService createProxy(Vertx vertx, String address)
	{
		return new VideoStreamService(new VideoStreamServiceVertxEBProxy(vertx.getDelegate(), address));
	}

	private final cz.klitea.recognitor.service.VideoStreamService delegate;

	public VideoStreamService(cz.klitea.recognitor.service.VideoStreamService delegate)
	{
		this.delegate = delegate;
	}

	public cz.klitea.recognitor.service.VideoStreamService getDelegate()
	{
		return delegate;
	}

	public Single<VideoStreamEntity> add(VideoStreamEntity entity)
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.add(entity, fut);
		}));
	}

	public Single<Boolean> remove(VideoStreamId id)
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.remove(id, fut);
		}));
	}

	public Single<List<VideoStreamEntity>> list()
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.list(fut);
		}));
	}

	public Single<VideoStreamEntity> single(VideoStreamId id)
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.single(id, fut);
		}));
	}

	public Single<List<VideoStreamClientEntity>> clients(VideoStreamId id)
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.clients(id, fut);
		}));
	}

	public Single<Boolean> disconnectClient(VideoStreamId streamId, VideoStreamClientId clientId)
	{
		return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
			delegate.disconnectClient(streamId, clientId, fut);
		}));
	}
}
