package cz.klitea.recognitor.rxjava.stream;

import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rx.java.RxHelper;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.streams.ReadStream;
import org.bytedeco.javacv.FrameGrabber;
import rx.Observable;

/**
 * RxJava Wrapper
 *
 * @author frantisek.cejka
 */
public class BlockingFrameMatrixReadStream implements ReadStream<FrameMatrix>
{
	final static Logger LOG = LoggerFactory.getLogger(BlockingFrameMatrixReadStream.class);

	public static BlockingFrameMatrixReadStream from(Vertx vertx, FrameGrabber grabber)
	{
		return new BlockingFrameMatrixReadStream(vertx, grabber);
	}

	private final io.vertx.core.streams.ReadStream delegate;

	private Observable<FrameMatrix> observable;


	public BlockingFrameMatrixReadStream(Vertx vertx, FrameGrabber grabber)
	{
		this.delegate = new cz.klitea.recognitor.stream.BlockingFrameMatrixReadStream(vertx.getDelegate(), grabber);
	}

	@Override
	public io.vertx.core.streams.ReadStream getDelegate()
	{
		return delegate;
	}

	@Override
	public ReadStream<FrameMatrix> exceptionHandler(Handler<Throwable> handler)
	{
		delegate.exceptionHandler(handler);
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> handler(Handler<FrameMatrix> handler)
	{
		delegate.handler(handler);
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> pause()
	{
		delegate.pause();
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> resume()
	{
		delegate.resume();
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> endHandler(Handler<Void> endHandler)
	{
		delegate.endHandler(endHandler);
		return this;
	}

	@Override
	public Observable<FrameMatrix> toObservable()
	{
		if (observable == null) {
			observable = RxHelper.toObservable(delegate);
		}

		return observable;
	}
}