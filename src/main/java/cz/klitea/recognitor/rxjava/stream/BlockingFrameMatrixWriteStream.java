package cz.klitea.recognitor.rxjava.stream;

import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.streams.WriteStream;
import org.bytedeco.javacv.FrameRecorder;

/**
 * RxJava Wrapper
 *
 * @author frantisek.cejka
 */
public class BlockingFrameMatrixWriteStream implements WriteStream<FrameMatrix>
{
	final static Logger LOG = LoggerFactory.getLogger(BlockingFrameMatrixWriteStream.class);

	public static BlockingFrameMatrixWriteStream from(Vertx vertx, FrameRecorder recorder)
	{
		return new BlockingFrameMatrixWriteStream(vertx, recorder);
	}

	private final io.vertx.core.streams.WriteStream delegate;

	public BlockingFrameMatrixWriteStream(Vertx vertx, FrameRecorder recorder)
	{
		this.delegate = new cz.klitea.recognitor.stream.BlockingFrameMatrixWriteStream(vertx.getDelegate(), recorder);
	}

	@Override
	public io.vertx.core.streams.WriteStream getDelegate()
	{
		return delegate;
	}

	@Override
	public WriteStream<FrameMatrix> exceptionHandler(Handler<Throwable> handler)
	{
		delegate.exceptionHandler(handler);
		return this;
	}

	@Override
	public WriteStream<FrameMatrix> write(FrameMatrix data)
	{
		delegate.write(data);
		return this;
	}

	@Override
	public void end()
	{
		delegate.end();
	}

	@Override
	public void end(FrameMatrix resourceFrame)
	{
		write(resourceFrame);
		end();
	}

	@Override
	public WriteStream<FrameMatrix> setWriteQueueMaxSize(int maxSize)
	{
		delegate.setWriteQueueMaxSize(maxSize);
		return this;
	}

	@Override
	public boolean writeQueueFull()
	{
		return delegate.writeQueueFull();
	}

	@Override
	public WriteStream<FrameMatrix> drainHandler(Handler<Void> handler)
	{
		delegate.drainHandler(handler);
		return this;
	}
}

