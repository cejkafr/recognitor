package cz.klitea.recognitor.rxjava.stream;

import cz.klitea.recognitor.entity.VideoStreamMetadata;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rx.java.RxHelper;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.streams.ReadStream;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import rx.Observable;
import rx.Single;

import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FrameMatrixReadStream implements ReadStream<FrameMatrix>
{
	final static Logger LOG = LoggerFactory.getLogger(FrameMatrixReadStream.class);

	public static Single<FrameMatrixReadStream> create(Vertx vertx, FrameGrabber grabber)
	{
		return vertx.rxExecuteBlocking(event -> {

			event.complete(new FrameMatrixReadStream(vertx,
					Executors.newSingleThreadExecutor(), grabber));

		});
	}

	public static Single<FrameMatrixReadStream> create(Vertx vertx, JsonObject json)
	{
		return vertx.rxExecuteBlocking(event -> {

			String resourceUrl = json.getString("resource-url");
			String format = json.getString("format", null);
			int timeout = json.getInteger("timeout", 5);

			FrameGrabber grabber;

			// local device
			if (resourceUrl.startsWith("devId://")) {

				try {
					int deviceId = Integer.parseInt(resourceUrl.substring("devId://".length()));
					grabber = new OpenCVFrameGrabber(deviceId);
				} catch (Exception ex) {
					event.fail(ex);
					return;
				}

				// remote device
			} else {

				grabber = new FFmpegFrameGrabber(resourceUrl);
				grabber.setAudioChannels(0);
				grabber.setAudioStream(0);
				grabber.setTimeout(timeout * 1000);
				grabber.setOption("stimeout", Integer.toString(timeout * 1000));
				if (resourceUrl.startsWith("rtsp://")) {
				} else if (format != null && format.equals("h264")) {
					grabber.setFormat("h264");
				}

			}

			event.complete(new FrameMatrixReadStream(vertx,
					Executors.newSingleThreadExecutor(), grabber));
		});
	}

	private final cz.klitea.recognitor.stream.FrameMatrixReadStream delegate;

	private Observable<FrameMatrix> observable;

	public FrameMatrixReadStream(Vertx vertx, ExecutorService executor, FrameGrabber grabber)
	{
		this.delegate = new cz.klitea.recognitor.stream.FrameMatrixReadStream(vertx.getDelegate(), executor, grabber);
	}

	@Override
	public io.vertx.core.streams.ReadStream getDelegate()
	{
		return delegate;
	}

	@Override
	public Observable<FrameMatrix> toObservable()
	{
		if (observable == null) {
			observable = RxHelper.toObservable(delegate);
		}

		return observable;
	}

	@Override
	public ReadStream<FrameMatrix> handler(Handler<FrameMatrix> handler)
	{
		delegate.handler(handler);
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> exceptionHandler(Handler<Throwable> handler)
	{
		delegate.exceptionHandler(handler);
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> endHandler(Handler<Void> endHandler)
	{
		delegate.endHandler(endHandler);
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> pause()
	{
		delegate.pause();
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> resume()
	{
		delegate.resume();
		return this;
	}

	public ReadStream<FrameMatrix> close()
	{
		delegate.close();
		return this;
	}

	public boolean isRunning()
	{
		return delegate.isRunning();
	}

	public boolean isUnavailable()
	{
		return delegate.isUnavailable();
	}

	public boolean isPaused()
	{
		return delegate.isPaused();
	}

	public boolean isClosed()
	{
		return delegate.isClosed();
	}

	public boolean isActive()
	{
		return delegate.isActive();
	}

	public long getLastFrameIndex()
	{
		return delegate.getLastFrameIndex();
	}

	public VideoStreamMetadata getMetadata()
	{
		return delegate.getMetadata();
	}

	public Instant getStartedAt()
	{
		return delegate.getStartedAt();
	}

	public Instant getUnavailableSince()
	{
		return delegate.getUnavailableSince();
	}
}