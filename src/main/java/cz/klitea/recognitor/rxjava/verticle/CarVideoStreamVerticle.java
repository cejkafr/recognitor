package cz.klitea.recognitor.rxjava.verticle;

import com.openalpr.jni.AlprPlate;
import com.openalpr.jni.AlprPlateResult;
import com.openalpr.jni.AlprResults;
import cz.klitea.recognitor.entity.VideoStreamMetadata;
import cz.klitea.recognitor.rxjava.service.CarLicencePlateService;
import cz.klitea.recognitor.rxjava.service.VideoStreamService;
import cz.klitea.recognitor.rxjava.stream.FrameMatrixReadStream;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.core.streams.ReadStream;
import io.vertx.rxjava.ext.web.Router;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.*;
import org.jetbrains.annotations.NotNull;
import rx.Observable;
import rx.Single;
import rx.Subscription;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;

import static org.bytedeco.javacpp.opencv_core.FONT_HERSHEY_PLAIN;
import static org.bytedeco.javacpp.opencv_imgproc.putText;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;

/**
 * This verticle will be running for every created video stream.
 * It starts new webserver on specified port and distributes data to clients.
 * <p>
 * There's gonna be some worker verticles to handle blocking IO, like decoding, face detection.
 *
 * @author fcejka
 */
public class CarVideoStreamVerticle extends AbstractVerticle
{
	final static Logger LOG = LoggerFactory.getLogger(CarVideoStreamVerticle.class);

	private VideoStreamService streamService;
	private CarLicencePlateService carPlateService;

	private FrameMatrixReadStream frameStream;
	private Observable<FrameMatrix> frameObservable;
	private Observable<AlprResults> licencePlateDetObservable;
	private Observable<FrameMatrix> frameLicencePlateDetObservable;

	private Instant startedAt;

	private OpenCVFrameConverter.ToMat converterToMat;

	private int licenceDetCount = 3;
	private int licenceDetIdx = licenceDetCount;

	@Override
	public void start(Future<Void> startFuture) throws Exception
	{
		LOG.info("Starting video stream verticle '{}'.", config());

		JsonObject config = new JsonObject()
				.put("resource-url", config().getString("uri"));

		FrameMatrixReadStream.create(vertx, config)
				.doOnSuccess(this::initServicesAndObservables)
				.flatMap(aVoid -> initRouter())
				.flatMap(this::startWebserver)
				.doOnSubscribe(() -> startedAt = Instant.now())
				.subscribe(httpServer -> startFuture.complete(), startFuture::fail);

	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception
	{
		LOG.info("Stopping video stream verticle.");

		stopFuture.complete();
	}

	private void initServicesAndObservables(FrameMatrixReadStream stream)
	{
		this.converterToMat = new OpenCVFrameConverter.ToMat();

		this.streamService = VideoStreamService.createProxy(
				vertx, WebServiceVerticle.VIDEO_STREAM_SERVICE);
		this.carPlateService = CarLicencePlateService.create(vertx);

		this.frameStream = stream;

		this.frameObservable = frameStream.toObservable().share();
		this.licencePlateDetObservable = frameObservable.flatMap(this::runLicencePlateDetection).share();
		this.frameLicencePlateDetObservable = frameObservable.withLatestFrom(
				this.licencePlateDetObservable, this::renderLicencePlates).share();

		this.licencePlateDetObservable.subscribe(alprResults ->
		{
			LOG.debug("Detected {} plates.", alprResults.getPlates().size());
			alprResults.getPlates().stream().forEach(plate -> {
				AlprPlate bestPlate = plate.getBestPlate();
				LOG.debug("Plate {} confidence {} loc {}.", bestPlate.getCharacters(),
						bestPlate.getOverallConfidence(), plate.getPlatePoints());
			});
		});
	}

	private Observable<AlprResults> runLicencePlateDetection(FrameMatrix frame)
	{
		if (licenceDetIdx >= licenceDetCount) {
			licenceDetIdx = 0;
			LOG.debug("Licence plate on frame #{}, after {}ms.",
					frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());
			return carPlateService.detectAndRead(frame).toObservable();
		} else {
			licenceDetIdx++;
			return Observable.empty();
		}
	}

	private FrameMatrix renderLicencePlates(FrameMatrix frame, AlprResults results)
	{
		if (results.getPlates().isEmpty())
			return frame;

		LOG.trace("Render licence plate detection in frame #{}, after {}ms.",
				frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());

		long ts = System.currentTimeMillis();

		opencv_core.Mat videoMat = frame.getData().clone();

		LOG.debug("Detected {} plates.", results.getPlates().size());
		results.getPlates().stream().forEach(plate ->  {
			AlprPlate bestPlate = plate.getBestPlate();
			LOG.debug("Plate {} confidence {}.", bestPlate.getCharacters(),
					bestPlate.getOverallConfidence());

			opencv_core.Rect face_i = new opencv_core.Rect(plate.getPlatePoints().get(0).getX(),
					plate.getPlatePoints().get(0).getY(), plate.getPlatePoints().get(1).getX() - plate.getPlatePoints().get(0).getX(),
					plate.getPlatePoints().get(2).getY() - plate.getPlatePoints().get(0).getY());
			rectangle(videoMat, face_i, new opencv_core.Scalar(0, 0, 255, 1));

			// Create the text we will annotate the box with:
			String box_text = bestPlate.getCharacters();
			// Calculate the position for annotated text (make sure we don't
			// put illegal values in there):
			int pos_x = Math.max(face_i.tl().x() - 20, 0);
			int pos_y = Math.max(face_i.tl().y() - 20, 0);
			// And now put it into the image:
			putText(videoMat, box_text, new opencv_core.Point(pos_x, pos_y),
					FONT_HERSHEY_PLAIN, 3.0, new opencv_core.Scalar(0, 255, 0, 2.0));
		});

		LOG.trace("Rendering in frame #{} took {}ms.",
				frame.getNumber(), System.currentTimeMillis() - ts);

		return new FrameMatrix(frame.getTimestamp(), frame.getTime(),
				frame.getNumber(), frame.getImageType(), frame.getData());
	}

	private Single<FFmpegFrameRecorder> createRecorder(OutputStream os)
	{
		return vertx.rxExecuteBlocking(event -> {
			LOG.info("Creating reader for {}.", frameStream.getMetadata());

			FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(os,
					frameStream.getMetadata().getImageWidth(), frameStream.getMetadata().getImageHeight());
			recorder.setInterleaved(true);
			recorder.setVideoOption("tune", "zerolatency");
			recorder.setVideoOption("preset", "ultrafast");
			recorder.setVideoOption("crf", "28");
			recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
			recorder.setAudioCodec(0);
			recorder.setAudioChannels(0);
			recorder.setFormat("mpegts");
			recorder.setFrameRate(frameStream.getMetadata().getFrameRate());
			try {
				recorder.start();
			} catch (IOException ex) {
				event.fail(ex);
			}
			event.complete(recorder);
		});
	}

	private Single<Void> disposeRecorder(final FFmpegFrameRecorder recorder)
	{
		if (recorder != null) {
			return vertx.rxExecuteBlocking(event -> {
				try {
					recorder.stop();
					recorder.release();
					event.complete();
				} catch (IOException ex) {
					event.fail(ex);
				}
			});
		} else {
			return Single.just(null);
		}
	}

	@NotNull
	private Single<Router> initRouter()
	{
		Router router = Router.router(vertx);

		router.get("/stream.ts").handler(ctx -> {
			ctx.response().setChunked(true)
					.putHeader("Content-Type", "video/mp2t;profiles=\"1\"");

			final ByteArrayOutputStream os
					= new ByteArrayOutputStream(2048);

			createRecorder(os).subscribe(recorder -> {
						final Subscription subs = frameLicencePlateDetObservable.subscribe(frame -> {
							if (!ctx.response().writeQueueFull()) {
								vertx.rxExecuteBlocking(event -> {
									final long ts = System.currentTimeMillis();
									try {
										LOG.trace("Encoding frame #{}, after {}ms.",
												frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());

										recorder.record(converterToMat.convert(frame.getData()));
										Buffer buffer = Buffer.buffer(os.size());
										buffer.getDelegate().appendBytes(os.toByteArray());
										os.reset();
										ctx.response().write(buffer);
										event.complete();

										LOG.trace("Frame #{} encoded in {}ms.",
												frame.getNumber(), System.currentTimeMillis() - ts);
									} catch (IOException ex) {
										event.fail(ex);
									}
								}, true).subscribe();
							}
						});

						ctx.response().closeHandler(event -> {
							LOG.debug("Closing cleanup.");
							subs.unsubscribe();
							disposeRecorder(recorder).subscribe();
					});
			});
		});


		return Single.just(router);
	}

	private Single<HttpServer> startWebserver(Router router)
	{
		JsonObject config = config().getJsonObject("webserver", new JsonObject());
		HttpServerOptions opts = new HttpServerOptions()
				.setPort(config.getInteger("port"))
				.setHost(config.getString("host", "localhost"));

		LOG.info("Starting webserver at {}:{}", opts.getHost(), opts.getPort());
		router.getRoutes().stream()
				.filter(route -> !(route.getPath().endsWith("/") && route.getPath().length() > 1))
				.forEach(route -> LOG.info("Request mapping {}", route.getPath()));

		return vertx.createHttpServer(opts)
				.requestHandler(router::accept)
				.rxListen();
	}
}
