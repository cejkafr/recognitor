package cz.klitea.recognitor.rxjava.verticle;

import cz.klitea.recognitor.rxjava.service.FaceDetectionService;
import cz.klitea.recognitor.rxjava.service.VideoStreamService;
import cz.klitea.recognitor.rxjava.stream.FrameMatrixReadStream;
import cz.klitea.recognitor.service.FaceDet;
import cz.klitea.recognitor.service.FaceDetectionConfig;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.ext.web.Router;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.*;
import org.jetbrains.annotations.NotNull;
import rx.Observable;
import rx.Single;
import rx.Subscription;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;

import static org.bytedeco.javacpp.opencv_imgproc.rectangle;

/**
 * This verticle will be running for every created video stream.
 * It starts new webserver on specified port and distributes data to clients.
 *
 * @author fcejka
 */
public class FaceVideoStreamVerticle extends AbstractVerticle
{
	final static Logger LOG = LoggerFactory.getLogger(FaceVideoStreamVerticle.class);
	final static OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();

	private VideoStreamService streamService;
	private FaceDetectionService faceDetService;

	private FrameMatrixReadStream frameStream;
	private Observable<FrameMatrix> frameObservable;
	private Observable<FaceDet.List> faceDetObservable;
	private Observable<FrameMatrix> frameFaceDetObservable;

	private Instant startedAt;

	private int faceDetCount = 4;
	private int faceDetIdx = faceDetCount;

	@Override
	public void start(Future<Void> startFuture) throws Exception
	{
		LOG.info("Starting video stream verticle '{}'.", config());

		JsonObject config = new JsonObject()
				.put("resource-url", config().getString("uri"));

		 FrameMatrixReadStream.create(vertx, config)
				.doOnSuccess(this::initServicesAndObservables)
				.flatMap(aVoid -> initRouter())
				.flatMap(this::startWebserver)
				.doOnSubscribe(() -> startedAt = Instant.now())
				.subscribe(httpServer -> startFuture.complete(), startFuture::fail);

	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception
	{
		LOG.info("Stopping video stream verticle.");

		stopFuture.complete();
	}

	private void initServicesAndObservables(FrameMatrixReadStream stream)
	{
		this.streamService = VideoStreamService.createProxy(
				vertx, WebServiceVerticle.VIDEO_STREAM_SERVICE);
		this.faceDetService = FaceDetectionService.create(vertx, new FaceDetectionConfig());

		this.frameStream = stream;

		this.frameObservable = frameStream.toObservable().share();
		this.faceDetObservable = frameObservable.flatMap(this::runFaceDetection).share();
		this.frameFaceDetObservable = frameObservable.withLatestFrom(
				this.faceDetObservable, this::renderFaceDetections).share();

		this.faceDetObservable.subscribe(rectVector -> {
			LOG.debug("Detected " + rectVector.getSize() + " faces.");
		});
	}

	private Observable<FaceDet.List> runFaceDetection(FrameMatrix frame)
	{
		if (faceDetIdx >= faceDetCount) {
			faceDetIdx = 0;
			LOG.debug("Face detection on frame #{}, after {}ms.",
					frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());
			return faceDetService.detect(frame).toObservable();
		} else {
			faceDetIdx++;
			return Observable.empty();
		}
	}

	private FrameMatrix renderFaceDetections(FrameMatrix frame, FaceDet.List rectVector)
	{
		if (rectVector.getSize() == 0)
			return frame;

		LOG.debug("Render face detection in frame #{}, after {}ms.",
				frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());

		long ts = System.currentTimeMillis();

		opencv_core.Mat videoMat = frame.getData().clone();

		for (int i = 0; i < rectVector.getSize(); i++) {
			FaceDet face_i = rectVector.getData().get(i);
			rectangle(videoMat, face_i.getRect(), new opencv_core.Scalar(0, 0, 255, 1));
		}

		LOG.trace("Rendering in frame #{} took {}ms.",
				frame.getNumber(), System.currentTimeMillis() - ts);

		return new FrameMatrix(frame.getTimestamp(), frame.getTime(),
				frame.getNumber(), frame.getImageType(), videoMat);
	}

	private Single<FFmpegFrameRecorder> createRecorder(OutputStream os)
	{
		return vertx.rxExecuteBlocking(event -> {
			LOG.info("Creating reader for {}.", frameStream.getMetadata());

			FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(os,
					frameStream.getMetadata().getImageWidth(),
					frameStream.getMetadata().getImageHeight());
			recorder.setInterleaved(true);
			recorder.setVideoOption("tune", "zerolatency");
			recorder.setVideoOption("preset", "ultrafast");
			recorder.setVideoOption("crf", "28");
			recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
			recorder.setAudioCodec(0);
			recorder.setAudioChannels(0);
			recorder.setFormat("mpegts");
			recorder.setFrameRate(frameStream.getMetadata().getFrameRate());
			try {
				recorder.start();
			} catch (IOException ex) {
				event.fail(ex);
			}
			event.complete(recorder);
		});
	}

	private Single<Void> disposeRecorder(final FFmpegFrameRecorder recorder)
	{
		if (recorder != null) {
			return vertx.rxExecuteBlocking(event -> {
				try {
					recorder.stop();
					recorder.release();
					event.complete();
				} catch (IOException ex) {
					event.fail(ex);
				}
			});
		} else {
			return Single.just(null);
		}
	}

	@NotNull
	private Single<Router> initRouter()
	{
		Router router = Router.router(vertx);

		router.get("/stream.ts").handler(ctx -> {
			ctx.response().setChunked(true)
					.putHeader("Content-Type", "video/mp2t;profiles=\"1\"");

			final ByteArrayOutputStream os
					= new ByteArrayOutputStream(2048);

			createRecorder(os).subscribe(recorder ->
			{
						final Subscription subs = frameFaceDetObservable.flatMapSingle(frame ->
						{
								return vertx.<Buffer>rxExecuteBlocking(event -> {
									final long ts = System.currentTimeMillis();
									try {
										LOG.debug("Encoding frame #{}, after {}ms.",
												frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());

										recorder.record(converter.convert(frame.getData()));
										Buffer buffer = Buffer.buffer(os.size());
										buffer.getDelegate().appendBytes(os.toByteArray());
										os.reset();
										event.complete(buffer);

										LOG.trace("Frame #{} encoded in {}ms.",
												frame.getNumber(), System.currentTimeMillis() - ts);
									} catch (IOException ex) {
										event.fail(ex);
									}
								}, true);

						}).subscribe(buffer -> {
							if (!ctx.response().writeQueueFull()) {
								ctx.response().write(buffer);
							}
						});

						ctx.response().closeHandler(event -> {
							LOG.debug("Closing cleanup.");
							subs.unsubscribe();
							disposeRecorder(recorder).subscribe();
						});
			});
		});


		return Single.just(router);
	}

	private Single<HttpServer> startWebserver(Router router)
	{
		JsonObject config = config().getJsonObject("webserver", new JsonObject());
		HttpServerOptions opts = new HttpServerOptions()
				.setPort(config.getInteger("port"))
				.setHost(config.getString("host", "localhost"));

		LOG.info("Starting webserver at {}:{}", opts.getHost(), opts.getPort());
		router.getRoutes().stream()
				.filter(route -> !(route.getPath().endsWith("/") && route.getPath().length() > 1))
				.forEach(route -> LOG.info("Request mapping {}", route.getPath()));

		return vertx.createHttpServer(opts)
				.requestHandler(router::accept)
				.rxListen();
	}
}
