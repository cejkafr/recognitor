package cz.klitea.recognitor.rxjava.verticle;

import cz.klitea.recognitor.entity.Area;
import cz.klitea.recognitor.rxjava.service.MotionDetectionService;
import cz.klitea.recognitor.rxjava.service.VideoStreamService;
import cz.klitea.recognitor.rxjava.stream.FrameMatrixReadStream;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.ext.web.Router;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.*;
import org.jetbrains.annotations.NotNull;
import rx.Observable;
import rx.Single;
import rx.Subscription;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;
import java.util.Set;

import static org.bytedeco.javacpp.opencv_imgproc.rectangle;

/**
 * This verticle will be running for every created video stream.
 * It starts new webserver on specified port and distributes data to clients.
 * <p>
 * There's gonna be some worker verticles to handle blocking IO, like decoding, face detection.
 *
 * @author fcejka
 */
public class MotionVideoStreamVerticle extends AbstractVerticle
{
	final static Logger LOG = LoggerFactory.getLogger(MotionVideoStreamVerticle.class);
	final static OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();

	private VideoStreamService streamService;
	private MotionDetectionService motionDetService;

	private FrameMatrixReadStream frameStream;
	private Observable<FrameMatrix> frameObservable;
	private Observable<Set<Integer>> motionDetObservable;
	private Observable<FrameMatrix> frameMotionDetObservable;

	private Instant startedAt;

	private Area[] sa;

	private int motionDetCount = 4;
	private int motionDetIdx = motionDetCount;

	@Override
	public void start(Future<Void> startFuture) throws Exception
	{
		LOG.info("Starting video stream verticle '{}'.", config());

		JsonObject config = new JsonObject()
				.put("resource-url", config().getString("uri"));

		FrameMatrixReadStream.create(vertx, config)
				.doOnSuccess(this::initServicesAndObservables)
				.flatMap(aVoid -> initRouter())
				.flatMap(this::startWebserver)
				.doOnSubscribe(() -> startedAt = Instant.now())
				.subscribe(httpServer -> startFuture.complete(), startFuture::fail);

	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception
	{
		LOG.info("Stopping video stream verticle.");

		stopFuture.complete();
	}

	private void initServicesAndObservables(FrameMatrixReadStream stream)
	{
		sa = new Area[100];

		int xUnit = frameStream.getMetadata().getImageWidth() / 10;
		int yUnit = frameStream.getMetadata().getImageHeight() / 10;

		int i = 0;
		for (int y = 0; y < 10; y++) {
			for (int x = 0; x < 10; x++) {
				sa[i++] = new Area(x * xUnit, y * yUnit, xUnit, yUnit);
			}
		}

		this.streamService = VideoStreamService.createProxy(
				vertx, WebServiceVerticle.VIDEO_STREAM_SERVICE);
		this.motionDetService = MotionDetectionService.create(vertx, sa);

		this.frameStream = stream;

		this.frameObservable = frameStream.toObservable().share();
		this.motionDetObservable = frameObservable.flatMap(this::runMotionDetection).share();
		this.frameMotionDetObservable = frameObservable.withLatestFrom(
				this.motionDetObservable, this::renderMotionDetections).share();

		this.motionDetObservable.subscribe(integers -> {
			LOG.debug("Motion in " + integers.size() + " areas.");
		});
	}

	private Observable<Set<Integer>> runMotionDetection(FrameMatrix frame)
	{
		if (motionDetIdx >= motionDetCount) {
			motionDetIdx = 0;
			LOG.debug("Motion detection on frame #{}, after {}ms.",
					frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());
			return motionDetService.detect(frame).toObservable();
		} else {
			motionDetIdx++;
			return Observable.empty();
		}
	}

	@NotNull
	private FrameMatrix renderMotionDetections(FrameMatrix frame, Set<Integer> cvSeq)
	{
		LOG.debug("Render motion detection in frame #{}, after {}ms.",
				frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());

		long ts = System.currentTimeMillis();

		opencv_core.Mat videoMat = frame.getData().clone();

		for (int i = 0; i < sa.length; i++) {
			Area area = sa[i];

			opencv_core.Rect rect = new opencv_core.Rect(area.x(), area.y(), area.width(), area.height());
			opencv_core.Scalar color;
			if (cvSeq.contains(i)) {
				color = new opencv_core.Scalar(255, 0, 0, 1);
			} else {
				color = new opencv_core.Scalar(0, 255, 0, 1);
			}
			rectangle(videoMat, rect, color);
		}
		;

		LOG.trace("Rendering in frame #{} took {}ms.",
				frame.getNumber(), System.currentTimeMillis() - ts);

		return new FrameMatrix(frame.getTimestamp(), frame.getTime(),
				frame.getNumber(), frame.getImageType(), videoMat);
	}

	private Single<FFmpegFrameRecorder> createRecorder(OutputStream os)
	{
		return vertx.rxExecuteBlocking(event -> {
			LOG.info("Creating reader for {}.", frameStream.getMetadata());

			FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(os,
					frameStream.getMetadata().getImageWidth(),
					frameStream.getMetadata().getImageHeight());
			recorder.setInterleaved(true);
			recorder.setVideoOption("tune", "zerolatency");
			recorder.setVideoOption("preset", "ultrafast");
			recorder.setVideoOption("crf", "28");
			recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
			recorder.setAudioCodec(0);
			recorder.setAudioChannels(0);
			recorder.setFormat("mpegts");
			recorder.setFrameRate(frameStream.getMetadata().getFrameRate());
			recorder.setVideoBitrate(2048);
			try {
				recorder.start();
			} catch (IOException ex) {
				event.fail(ex);
			}
			event.complete(recorder);
		});
	}

	private Single<Void> disposeRecorder(final FFmpegFrameRecorder recorder)
	{
		if (recorder != null) {
			return vertx.rxExecuteBlocking(event -> {
				try {
					recorder.stop();
					recorder.release();
					event.complete();
				} catch (IOException ex) {
					event.fail(ex);
				}
			});
		} else {
			return Single.just(null);
		}
	}

	@NotNull
	private Single<Router> initRouter()
	{
		Router router = Router.router(vertx);

		router.get("/stream.ts").handler(ctx -> {
			ctx.response().setChunked(true)
					.putHeader("Content-Type", "video/mp2t;profiles=\"1\"");

			final ByteArrayOutputStream os
					= new ByteArrayOutputStream(2048);

			createRecorder(os).subscribe(recorder ->
			{
				final Subscription subs = frameMotionDetObservable.flatMapSingle(frame ->
				{
					return vertx.<Buffer>rxExecuteBlocking(event -> {
						final long ts = System.currentTimeMillis();
						try {
							LOG.debug("Encoding frame #{}, after {}ms.",
									frame.getNumber(), System.currentTimeMillis() - frame.getTimestamp());

							recorder.record(converter.convert(frame.getData()));
							Buffer buffer = Buffer.buffer(os.size());
							buffer.getDelegate().appendBytes(os.toByteArray());
							os.reset();
							event.complete(buffer);

							LOG.trace("Frame #{} encoded in {}ms.",
									frame.getNumber(), System.currentTimeMillis() - ts);
						} catch (IOException ex) {
							event.fail(ex);
						}
					}, true);

				}).subscribe(buffer -> {
					if (!ctx.response().writeQueueFull()) {
						ctx.response().write(buffer);
					}
				});

				ctx.response().closeHandler(event -> {
					LOG.debug("Closing cleanup.");
					subs.unsubscribe();
					disposeRecorder(recorder).subscribe();
				});
			});
		});


		return Single.just(router);
	}

	private Single<HttpServer> startWebserver(Router router)
	{
		JsonObject config = config().getJsonObject("webserver", new JsonObject());
		HttpServerOptions opts = new HttpServerOptions()
				.setPort(config.getInteger("port"))
				.setHost(config.getString("host", "localhost"));

		LOG.info("Starting webserver at {}:{}", opts.getHost(), opts.getPort());
		router.getRoutes().stream()
				.filter(route -> !(route.getPath().endsWith("/") && route.getPath().length() > 1))
				.forEach(route -> LOG.info("Request mapping {}", route.getPath()));

		return vertx.createHttpServer(opts)
				.requestHandler(router::accept)
				.rxListen();
	}
}
