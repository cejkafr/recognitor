package cz.klitea.recognitor.rxjava.verticle;

import cz.klitea.recognitor.entity.VideoStreamClientId;
import cz.klitea.recognitor.entity.VideoStreamEntity;
import cz.klitea.recognitor.entity.VideoStreamId;
import cz.klitea.recognitor.rxjava.service.VideoStreamService;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.healthchecks.Status;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.ext.healthchecks.HealthCheckHandler;
import io.vertx.rxjava.ext.healthchecks.HealthChecks;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.handler.*;
import io.vertx.rxjava.ext.web.sstore.LocalSessionStore;
import io.vertx.rxjava.ext.web.templ.ThymeleafTemplateEngine;
import io.vertx.serviceproxy.ProxyHelper;
import org.jetbrains.annotations.NotNull;
import rx.Single;

import java.time.Instant;

/**
 * Entry point to application, starts and registers VideoStreamService,
 * which deploys new video streams added through REST API.
 *
 * Theoretically, VideoStreamService should run in own Verticle,
 * but in this example its good enough as it is.
 *
 * @author fcejka
 */
public class WebServiceVerticle extends AbstractVerticle
{
	final static Logger LOG = LoggerFactory.getLogger(WebServiceVerticle.class);
	final static int KB = 1024;
	final static int MB = 1024 * KB;

	// Channel on which video stream service listens.
	public final static String VIDEO_STREAM_SERVICE = "video-stream.service";

	private VideoStreamService videoStreamService;

	// Some variables for health checks.
	private Instant startedAt;
	private int reqsSinceStart;

	@Override
	public void start(Future<Void> startFuture) throws Exception
	{
		LOG.info("Starting web service verticle '{}'.", config());

		// Create and register video stream as proxy service, to be available across application.
		videoStreamService = VideoStreamService.create(vertx);
		ProxyHelper.registerService(cz.klitea.recognitor.service.VideoStreamService.class,
				vertx.getDelegate(), videoStreamService.getDelegate(), VIDEO_STREAM_SERVICE);

		// Configure router and start service webserver.
		initRouter().flatMap(this::enableHealth)
				.flatMap(this::enableAPI)
				.flatMap(this::enableWeb)
				.flatMap(this::startWebserver)
				.doOnSubscribe(() -> {
					this.startedAt = Instant.now();
					this.reqsSinceStart = 0;
				})
				.subscribe(httpServer -> startFuture.complete(), startFuture::fail);
	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception
	{
		LOG.info("Stopping web service verticle.");

		stopFuture.complete();
	}

	/**
	 * Only to make start more readable.
	 * @return
	 */
	@NotNull
	private Single<Router> initRouter()
	{
		return Single.just(Router.router(vertx));
	}

	/**
	 * Defines available health checks.
	 * @param router
	 * @return
	 */
	@NotNull
	private Single<Router> enableHealth(Router router)
	{
		final HealthCheckHandler healthHandler = HealthCheckHandler
				.createWithHealthChecks(HealthChecks.create(vertx));

		healthHandler.register("webservice", future -> {
			future.complete(Status.OK(new JsonObject().put("startedAt", startedAt)
					.put("reqsSinceStart", reqsSinceStart)));
		});

		router.get("/health").handler(healthHandler);

		return Single.just(router);
	}

	/**
	 * Defines REST API.
	 * Since it's small API, I leave it here, close to service.
	 * @param router
	 * @return
	 */
	@NotNull
	private Single<Router> enableAPI(Router router)
	{
		// Global configuration for whole API.
		router.route("/api/*").handler(BodyHandler.create().setBodyLimit(1 * MB));
		router.route("/api/*").handler(ctx -> {
			ctx.addHeadersEndHandler(event -> {
				ctx.response()
						.putHeader("Content-Type", "text/html;charset=UTF-8");
			});

			ctx.addBodyEndHandler(event -> reqsSinceStart++);

			ctx.next();
		});

		// GET LIST of added video streams.
		router.get("/api/v1/video-streams").handler(ctx -> {
			videoStreamService.list().subscribe(
					entities -> ctx.response().end(new JsonArray(entities).encode()),
					throwable -> ctx.fail(throwable));
		});

		// ADD SINGLE video stream.
		router.post("/api/v1/video-streams").handler(ctx -> {
			VideoStreamEntity entity = VideoStreamEntity.fromString(ctx.getBodyAsString());
			// TODO: validations

			videoStreamService.add(entity)
					.subscribe(startedEntity -> {
						ctx.response().setStatusCode(201);
						ctx.response().putHeader("Location",
								String.format("/api/v1/video-streams/%s", startedEntity.getId().getValue()));
						ctx.response().end(startedEntity.toString());
					}, throwable -> ctx.fail(throwable));
		});

		// GET SINGLE video stream by its ID.
		router.get("/api/v1/video-streams/:id").handler(ctx -> {
			String reqId = ctx.pathParam("id");
			videoStreamService.single(new VideoStreamId(reqId))
					.subscribe(startedEntity -> {
						if (startedEntity != null) {
							ctx.response().end(startedEntity.toString());
						} else {
							ctx.response().setStatusCode(404).end();
						}
					}, throwable -> ctx.fail(throwable));
		});

		// REMOVE SINGLE video stream by its ID.
		router.delete("/api/v1/video-streams/:id").handler(ctx -> {
			String reqId = ctx.pathParam("id");
			videoStreamService.remove(new VideoStreamId(reqId))
					.subscribe(succeeded -> {
						if (succeeded) {
							ctx.response().end();
						} else {
							ctx.response().setStatusCode(404).end();
						}
					}, throwable -> ctx.fail(throwable));
		});

		// GET LIST of connected clients to a stream
		router.get("/api/v1/video-streams/:streamId/clients").handler(ctx -> {
			String streamId = ctx.pathParam("streamId");
			videoStreamService.clients(new VideoStreamId(streamId))
					.subscribe(entities -> {
						if (entities != null) {
							ctx.response().end(new JsonArray(entities).encode());
						} else {
							ctx.response().setStatusCode(404).end();
						}
					}, throwable -> ctx.fail(throwable));
		});

		// DISCONNECT SINGLE client by his ID.
		router.delete("/api/v1/video-streams/:streamId/clients/:clientId").handler(ctx -> {
			String streamId = ctx.pathParam("streamId");
			String clientId = ctx.pathParam("clientId");
			videoStreamService.disconnectClient(new VideoStreamId(streamId), new VideoStreamClientId(clientId))
					.subscribe(succeeded -> {
						if (succeeded) {
							ctx.response().end();
						} else {
							ctx.response().setStatusCode(404).end();
						}
					}, throwable -> ctx.fail(throwable));
		});

		return Single.just(router);
	}

	/**
	 * Definitions for web
	 * @param router
	 * @return
	 */
	@NotNull
	private Single<Router> enableWeb(Router router)
	{
		ThymeleafTemplateEngine templateEngine = ThymeleafTemplateEngine.create();

		// TODO: CSRF security for forms

		// I actually don't use neither cookies or sessions, maybe in future.
		router.route("/html/*").handler(CookieHandler.create());
		router.route("/html/*").handler(SessionHandler
			.create(LocalSessionStore.create(vertx))
						.setCookieHttpOnlyFlag(true)
						.setCookieSecureFlag(true));

		// Some security headers
		router.route("/html/*").handler(ctx -> {
					ctx.addHeadersEndHandler(event -> {
						ctx.response()
								.putHeader("Content-Type", "text/html;charset=UTF-8")
								// do not allow proxies to cache the data
								.putHeader("Cache-Control", "no-store, no-cache")
								// prevents Internet Explorer from MIME - sniffing a
								// response away from the declared contentType-type
								.putHeader("X-Content-Type-Options", "nosniff")
								// Strict HTTPS (for about ~6Months)
								.putHeader("Strict-Transport-Security", "max-age=" + 15768000)
								// IE8+ do not allow opening of attachments in the context of this resource
								.putHeader("X-Download-Options", "noopen")
								// enable XSS for IE
								.putHeader("X-XSS-Protection", "1; mode=block")
								// deny frames
								.putHeader("X-FRAME-OPTIONS", "DENY");
					});

					ctx.next();
		});

		router.route("/html/*").failureHandler(ErrorHandler.create());

		router.route("/html/index.html").handler(ctx ->
			templateEngine.rxRender(ctx, "webroot/html/layout.html")
					.subscribe(ctx.response()::end, ctx::fail));

		router.get("/static/*").handler(StaticHandler.create().setWebRoot("webroot/static"));

		router.get("/").handler(ctx -> ctx.reroute("/html/index.html"));

		return Single.just(router);
	}

	/**
	 * Exactly as it says.
	 * @param router
	 * @return
	 */
	private Single<HttpServer> startWebserver(Router router)
	{
		JsonObject config = config().getJsonObject("webserver", new JsonObject());
		HttpServerOptions opts = new HttpServerOptions()
				.setPort(config.getInteger("port", 8080))
				.setHost(config.getString("host", "localhost"));

		LOG.info("Starting webserver at {}:{}", opts.getHost(), opts.getPort());
		router.getRoutes().stream()
				.filter(route -> !(route.getPath().endsWith("/") && route.getPath().length() > 1))
				.forEach(route -> LOG.info("Request mapping {}", route.getPath()));

		return vertx.createHttpServer(opts)
				.requestHandler(router::accept)
				.rxListen();
	}
}
