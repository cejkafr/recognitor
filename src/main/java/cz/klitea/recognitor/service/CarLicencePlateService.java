package cz.klitea.recognitor.service;

import com.openalpr.jni.AlprResults;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import org.jetbrains.annotations.NotNull;

public interface CarLicencePlateService
{
	@NotNull
	static CarLicencePlateService create(Vertx vertx)
	{
		return new CarLicencePlateServiceImpl(vertx);
	}

	void detectAndRead(FrameMatrix frame, Handler<AsyncResult<AlprResults>> resultHandler);
}
