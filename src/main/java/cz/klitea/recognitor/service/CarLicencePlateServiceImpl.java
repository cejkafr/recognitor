package cz.klitea.recognitor.service;

import com.openalpr.jni.Alpr;
import com.openalpr.jni.AlprException;
import com.openalpr.jni.AlprResults;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.Pointer;
import org.bytedeco.javacpp.opencv_core;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.bytedeco.javacpp.opencv_core.CV_8U;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;

public class CarLicencePlateServiceImpl implements CarLicencePlateService
{
	final static Logger LOG = LoggerFactory.getLogger(CarLicencePlateServiceImpl.class);

	private final Vertx vertx;
	private final Alpr alpr;
	private final ByteArrayOutputStream os;

	//private opencv_core.Mat mat = new opencv_core.Mat();

	public CarLicencePlateServiceImpl(Vertx vertx)
	{
		this.vertx = vertx;
		this.alpr = new Alpr("eu", "/etc/openalpr/openalpr.conf",
				"/usr/share/openalpr/runtime_data");
		this.os = new ByteArrayOutputStream();

		if (!this.alpr.isLoaded()) {
			throw new RuntimeException("Alpr is not loaded.");
		}
	}

	@Override
	public void detectAndRead(FrameMatrix frame, Handler<AsyncResult<AlprResults>> resultHandler)
	{
		try {
			opencv_core.Mat mat = new opencv_core.Mat(
					frame.getData().arrayWidth(),
					frame.getData().arrayHeight(),
					CV_BGR2GRAY
			);

			cvtColor(frame.getData(), mat, CV_BGR2GRAY);
			AlprResults res = alpr.recognize(mat.address(), mat.arrayChannels(),
					mat.arrayWidth(), mat.arrayHeight());
			resultHandler.handle(Future.succeededFuture(res));
		} catch (AlprException ex)
		{
			resultHandler.handle(Future.failedFuture(ex));
		}
/*
		vertx.<AlprResults>executeBlocking(event ->
		{
			try {
				long ts = System.currentTimeMillis();
				long frameNo = frame.getNumber();

				//ImageIO.write(frame.toBufferedImage(), "png", os);

				//LOG.debug("PNG conversion for frame #{} took {}ms.",
				//		frameNo, ((System.currentTimeMillis() - ts) / 1000.0));

				//AlprResults res = alpr.recognize(os.toByteArray());

				opencv_core.Mat mat = new opencv_core.Mat(
						frame.getData().arrayWidth(),
						frame.getData().arrayHeight(),
						CV_BGR2GRAY
				);

				cvtColor(frame.getData(), mat, CV_BGR2GRAY);


				AlprResults res = alpr.recognize(mat.address(), mat.arrayChannels(),
						mat.arrayWidth(), mat.arrayHeight());
				event.complete(res);
				//os.reset();

				LOG.debug("Car plate detection for frame #{} took {}ms.",
						frameNo, ((System.currentTimeMillis() - ts) / 1000.0));
			} catch (AlprException ex) {
				event.fail(ex);
			}
		}, true, event -> {
			if (event.succeeded()) {
				resultHandler.handle(Future.succeededFuture(event.result()));
			} else {
				resultHandler.handle(Future.failedFuture(event.cause()));
			}
		});*/
	}
}
