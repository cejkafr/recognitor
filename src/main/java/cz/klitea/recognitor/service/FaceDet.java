package cz.klitea.recognitor.service;

import org.bytedeco.javacpp.opencv_core;

import java.util.ArrayList;

public class FaceDet
{
	public static class List
	{
		private java.util.List<FaceDet> data = new ArrayList<>();
		private int size;

		public java.util.List<FaceDet> getData()
		{
			return data;
		}

		public void setData(java.util.List<FaceDet> data)
		{
			this.data = data;
		}

		public int getSize()
		{
			return size;
		}

		public void setSize(int size)
		{
			this.size = size;
		}
	}

	private opencv_core.Rect rect;

	public FaceDet()
	{
	}

	public FaceDet(opencv_core.Rect rect)
	{
		this.rect = rect;
	}

	public opencv_core.Rect getRect()
	{
		return rect;
	}

	public void setRect(opencv_core.Rect rect)
	{
		this.rect = rect;
	}

	public opencv_core.Point getTl()
	{
		return rect.tl();
	}

	public opencv_core.Point getBr()
	{
		return rect.br();
	}

	public int getX()
	{
		return rect.x();
	}

	public FaceDet setX(int x)
	{
		rect.x(x);
		return this;
	}

	public int getY()
	{
		return rect.y();
	}

	public FaceDet setY(int y)
	{
		rect.y(y);
		return this;
	}

	public int getWidth()
	{
		return rect.width();
	}

	public FaceDet setWidth(int width)
	{
		rect.width(width);
		return this;
	}

	public int getHeight()
	{
		return rect.height();
	}

	public FaceDet setHeight(int height)
	{
		rect.height(height);
		return this;
	}
}
