package cz.klitea.recognitor.service;

import org.bytedeco.javacpp.opencv_core;

public class FaceDetectionConfig
{
	private String cascadeFile = "haarcascade_frontalface_default.xml";
	private opencv_core.Size minSize = new opencv_core.Size(100, 100);
	private opencv_core.Size maxSize = new opencv_core.Size(400, 400);
	private float scaleFactor = 1.3f;
	private int minNeighbors = 5;
	private int flags = 0;

	public FaceDetectionConfig()
	{
	}

	public FaceDetectionConfig(String cascadeFile)
	{
		this.cascadeFile = cascadeFile;
	}

	public String getCascadeFile()
	{
		return cascadeFile;
	}

	public FaceDetectionConfig setCascadeFile(String cascadeFile)
	{
		this.cascadeFile = cascadeFile;
		return this;
	}

	public opencv_core.Size getMinSize()
	{
		return minSize;
	}

	public FaceDetectionConfig setMinSize(int size)
	{
		this.minSize = new opencv_core.Size(size, size);
		return this;
	}

	public opencv_core.Size getMaxSize()
	{
		return maxSize;
	}

	public FaceDetectionConfig setMaxSize(int size)
	{
		this.minSize = new opencv_core.Size(size, size);
		return this;
	}

	public float getScaleFactor()
	{
		return scaleFactor;
	}

	public FaceDetectionConfig setScaleFactor(float scaleFactor)
	{
		this.scaleFactor = scaleFactor;
		return this;
	}

	public int getMinNeighbors()
	{
		return minNeighbors;
	}

	public FaceDetectionConfig setMinNeighbors(int minNeighbors)
	{
		this.minNeighbors = minNeighbors;
		return this;
	}

	public int getFlags()
	{
		return flags;
	}

	public FaceDetectionConfig setFlags(int flags)
	{
		this.flags = flags;
		return this;
	}
}
