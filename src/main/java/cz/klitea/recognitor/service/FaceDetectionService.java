package cz.klitea.recognitor.service;

import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import org.jetbrains.annotations.NotNull;

public interface FaceDetectionService
{
	@NotNull
	static FaceDetectionService create(Vertx vertx, FaceDetectionConfig config)
	{
		return new FaceDetectionServiceImpl(vertx, config);
	}

	void detect(FrameMatrix frame, Handler<AsyncResult<FaceDet.List>> resultHandler);
}
