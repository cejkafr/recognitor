package cz.klitea.recognitor.service;

import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_objdetect;

import java.io.*;

import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGRA2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.equalizeHist;

public class FaceDetectionServiceImpl implements FaceDetectionService
{
	final static Logger LOG = LoggerFactory.getLogger(FaceDetectionServiceImpl.class);

	private final Vertx vertx;
	private final FaceDetectionConfig config;

	private opencv_objdetect.CascadeClassifier cascade;
	private opencv_core.Mat videoMatGray;
	private opencv_core.RectVector faces;
	private FaceDet.List detections;

	public FaceDetectionServiceImpl(Vertx vertx, FaceDetectionConfig config)
	{
		this.vertx = vertx;
		this.config = config;

		this.cascade = new opencv_objdetect.CascadeClassifier(loadCascadeFile());
		this.videoMatGray = new opencv_core.Mat();

		this.faces = new opencv_core.RectVector();
		this.detections = new FaceDet.List();
	}

	@Override
	public void detect(FrameMatrix frame, Handler<AsyncResult<FaceDet.List>> resultHandler)
	{
		final long ts = System.currentTimeMillis();
		vertx.<opencv_core.RectVector>executeBlocking(
				event -> blockingDetect(frame, event),
				true,
				event -> {
					if (event.succeeded()) {
						resultHandler.handle(Future.succeededFuture(
								handleResult(event.result(), frame.getTime())));
					} else {
						resultHandler.handle(Future.failedFuture(event.cause()));
					}

					LOG.debug("Face detection for frame #{} took {}ms.",
							frame.getNumber(), ((System.currentTimeMillis() - ts) / 1000.0));
				});
	}


	private String loadCascadeFile()
	{
		if (config.getCascadeFile().startsWith("/"))
			return config.getCascadeFile();

		try {
			File file = File.createTempFile("cascade", ".xml");
			InputStream is = ClassLoader.getSystemResourceAsStream(config.getCascadeFile());
			FileOutputStream os = new FileOutputStream(file);

			byte[] buffer = new byte[1024];
			while (true) {
				int bytesRead = is.read(buffer);
				if (bytesRead == -1)
					break;
				os.write(buffer, 0, bytesRead);
			}

			return file.getAbsolutePath();
		} catch (IOException ex) {
			throw new RuntimeException("Failed to export cascade file.");
		}
	}

	private void blockingDetect(FrameMatrix frame, Future<opencv_core.RectVector> event)
	{
		try {
			cvtColor(frame.getData(), videoMatGray, COLOR_BGRA2GRAY);
			equalizeHist(videoMatGray, videoMatGray);

			cascade.detectMultiScale
					(
							videoMatGray,
							faces,
							config.getScaleFactor(),
							config.getMinNeighbors(),
							config.getFlags(),
							config.getMinSize(),
							config.getMaxSize()
					);

			event.complete(faces);
		} catch (Exception ex)
		{
			event.fail(ex);
		}
	}

	private FaceDet.List handleResult(opencv_core.RectVector faces, double time)
	{
		FaceDet d;
		for (int i = 0; i < faces.size(); i++) {
			opencv_core.Rect rect = faces.get(i);
			if (i >= detections.getSize()) {
				d = new FaceDet();
				detections.getData().add(d);
			} else {
				d = detections.getData().get(i);
			}

			d.setRect(rect);
		}

		detections.setSize((int) faces.size());

		return detections;
	}
}
