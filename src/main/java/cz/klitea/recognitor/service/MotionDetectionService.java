package cz.klitea.recognitor.service;

import cz.klitea.recognitor.entity.Area;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

public interface MotionDetectionService
{
	@NotNull
	static MotionDetectionService create(Vertx vertx, Area[] sa)
	{
		return new MotionDetectionServiceImpl(vertx, sa);
	}

	void detect(FrameMatrix frame, Handler<AsyncResult<Set<Integer>>> resultHandler);
}
