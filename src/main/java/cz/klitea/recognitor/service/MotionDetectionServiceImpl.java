package cz.klitea.recognitor.service;

import cz.klitea.recognitor.entity.Area;
import cz.klitea.recognitor.stream.FrameMatrix;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;

import java.util.HashSet;
import java.util.Set;

import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.cvAbsDiff;
import static org.bytedeco.javacpp.opencv_imgproc.*;

public class MotionDetectionServiceImpl implements MotionDetectionService
{
	final static Logger LOG = LoggerFactory.getLogger(MotionDetectionServiceImpl.class);

	private final Vertx vertx;
	private final Area[] sa;

	private opencv_core.CvMemStorage storage;
	private opencv_core.IplImage currFrame;
	private opencv_core.IplImage image;
	private opencv_core.IplImage prevImage = null;
	private opencv_core.IplImage diff = null;
	private opencv_core.CvSeq contour;
	private Set<Integer> az;

	public MotionDetectionServiceImpl(Vertx vertx, Area[] sa)
	{
		this.vertx = vertx;
		this.sa = sa;

		this.storage = opencv_core.CvMemStorage.create();
	}

	@Override
	public void detect(FrameMatrix frame, Handler<AsyncResult<Set<Integer>>> resultHandler)
	{
		final long ts = System.currentTimeMillis();

		vertx.<Set<Integer>>executeBlocking(event -> {
			currFrame = new opencv_core.IplImage(frame.getData());
			az = new HashSet<>();

			cvSmooth(currFrame, currFrame, CV_GAUSSIAN, 9, 9, 2, 2);
			if (image == null) {
				image = opencv_core.IplImage.create(currFrame.width(),
						currFrame.height(), IPL_DEPTH_8U, 1);
				cvCvtColor(currFrame, image, CV_RGB2GRAY);
			} else {
				prevImage = opencv_core.IplImage.create(currFrame.width(),
						currFrame.height(), IPL_DEPTH_8U, 1);
				prevImage = image;
				image = opencv_core.IplImage.create(currFrame.width(),
						currFrame.height(), IPL_DEPTH_8U, 1);
				cvCvtColor(currFrame, image, CV_RGB2GRAY);
			}

			if (diff == null) {
				diff = opencv_core.IplImage.create(currFrame.width(),
						currFrame.height(), IPL_DEPTH_8U, 1);
			}

			if (prevImage != null) {
				// perform ABS difference
				cvAbsDiff(image, prevImage, diff);
				// do some threshold for wipe away useless details
				cvThreshold(diff, diff, 64, 255, CV_THRESH_BINARY);

				// recognize contours
				contour = new opencv_core.CvSeq(null);
				cvFindContours(diff, storage, contour, Loader.sizeof(
						opencv_core.CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

				while (contour != null && !contour.isNull()) {
					if (contour.elem_size() > 0) {
						opencv_core.CvBox2D box = cvMinAreaRect2(contour, storage);
						if (box != null) {
							opencv_core.CvPoint2D32f center = box.center();
							opencv_core.CvSize2D32f size = box.size();

							for (int i = 0; i < sa.length; i++) {
								if ((Math.abs(center.x() - (sa[i].x() + sa[i].width() / 2))) < ((size.width() / 2) + (sa[i].width() / 2)) &&
										(Math.abs(center.y() - (sa[i].y() + sa[i].height() / 2))) < ((size.height() / 2) + (sa[i].height() / 2))) {

									az.add(i);

									/*System.out.println("Motion Detected in the area no: " + i +
											" Located at points: (" + sa[i].x() + ", " + sa[i].y() + ") -"
											+ " (" + (sa[i].x() +sa[i].width()) + ", "
											+ (sa[i].y() + sa[i].height()) + ")");*/
								}
							}
						}
					}

					contour = contour.h_next();
				}


				event.complete(az);
			}

		}, true, event -> {
			if (event.succeeded()) {
				resultHandler.handle(Future.succeededFuture(event.result()));
			} else {
				resultHandler.handle(Future.failedFuture(event.cause()));
			}

			LOG.debug("Motion detection for frame #{} took {}ms.",
					frame.getNumber(), ((System.currentTimeMillis() - ts) / 1000.0));
		});
	}
}
