package cz.klitea.recognitor.service;

import cz.klitea.recognitor.entity.VideoStreamClientEntity;
import cz.klitea.recognitor.entity.VideoStreamClientId;
import cz.klitea.recognitor.entity.VideoStreamEntity;
import cz.klitea.recognitor.entity.VideoStreamId;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Handles logic behind video streams. Stores them, runs verticles,
 * takes evidence of connected clients.
 *
 * TODO: At the moment, all data are stored in vertx local/cluster map.
 * TODO: But it would be nice to pass storage engine in the constructor, if even for testing purposes.
 *
 * @author fcejka
 */
@ProxyGen
public interface VideoStreamService
{
	@NotNull
	static VideoStreamService create(Vertx vertx)
	{
		return new VideoStreamServiceImpl(vertx);
	}

	@NotNull
	static VideoStreamService createProxy(Vertx vertx, String address)
	{
		return new cz.klitea.recognitor.service.VideoStreamServiceVertxEBProxy(vertx, address);
	}

	/**
	 * Adds new video stream, starts verticle.
	 * @param entity
	 * @param resultHandler
	 */
	void add(VideoStreamEntity entity, Handler<AsyncResult<VideoStreamEntity>> resultHandler);

	/**
	 * Removes existing video stream, stops verticle, returns FALSE on unknown ID.
	 * @param id
	 * @param resultHandler
	 */
	void remove(VideoStreamId id, Handler<AsyncResult<Boolean>> resultHandler);

	/**
	 * Lists all added video streams.
	 * @param resultHandler
	 */
	void list(Handler<AsyncResult<List<VideoStreamEntity>>> resultHandler);

	/**
	 * Gets video stream by its ID, returns null on unknown ID.
	 * @param id
	 * @param resultHandler
	 */
	void single(VideoStreamId id, Handler<AsyncResult<VideoStreamEntity>> resultHandler);

	/**
	 * Returns all connected clients to video stream.
	 * @param id
	 * @param resultHandler
	 */
	void clients(VideoStreamId id, Handler<AsyncResult<List<VideoStreamClientEntity>>> resultHandler);

	/**
	 * Disconnect client from video stream.
	 * @param streamId
	 * @param clientId
	 * @param resultHandler
	 */
	void disconnectClient(VideoStreamId streamId, VideoStreamClientId clientId, Handler<AsyncResult<Boolean>> resultHandler);
}
