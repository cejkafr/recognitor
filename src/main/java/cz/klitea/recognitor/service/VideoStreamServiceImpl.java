package cz.klitea.recognitor.service;

import cz.klitea.recognitor.entity.VideoStreamClientEntity;
import cz.klitea.recognitor.entity.VideoStreamClientId;
import cz.klitea.recognitor.entity.VideoStreamEntity;
import cz.klitea.recognitor.entity.VideoStreamId;
import cz.klitea.recognitor.rxjava.verticle.CarVideoStreamVerticle;
import cz.klitea.recognitor.rxjava.verticle.FaceVideoStreamVerticle;
import cz.klitea.recognitor.rxjava.verticle.MotionVideoStreamVerticle;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;

import java.util.List;
import java.util.Map;

/**
 * To be honest, implementation is little messy atm. I'm not used to write with handlers.
 * But since I can't use vertx codegen to generate RX service, creating this with wrapper seems like only solution.
 *
 * @author fcejka
 */
public class VideoStreamServiceImpl implements VideoStreamService
{
	private final Vertx vertx;

	public VideoStreamServiceImpl(Vertx vertx)
	{
		this.vertx = vertx;
	}

	private Map<JsonObject, JsonObject> localMap()
	{
		return vertx.sharedData().getLocalMap("video-streams");
	}

	private Map<JsonObject, JsonObject> clientLocalMap(VideoStreamId id)
	{
		return vertx.sharedData().getLocalMap(String.format("video-streams.%s.clients", id));
	}

	private Map<JsonObject, String> verticleLocalMap()
	{
		return vertx.sharedData().getLocalMap("video-streams.verticles");
	}

	@Override
	public void add(VideoStreamEntity entity, Handler<AsyncResult<VideoStreamEntity>> resultHandler)
	{
		if (vertx.isClustered()) {

		} else {
			entity.setId(new VideoStreamId());

			DeploymentOptions opts = new DeploymentOptions();
			opts.setConfig(new JsonObject()
					.put("name", entity.getName())
					.put("type", entity.getType())
					.put("uri", entity.getUri())
			.put("webserver", new JsonObject().put("port", entity.getStreamPort())));

			vertx.deployVerticle(FaceVideoStreamVerticle.class.getName(), opts, res ->
			{
				if (res.succeeded()) {
					vertx.sharedData().getLocalMap("video-streams")
							.put(entity.getId().toJson(), entity.toJson());
					vertx.sharedData().getLocalMap("video-streams.verticles")
							.put(entity.getId().toJson(), res.result());
					resultHandler.handle(Future.succeededFuture(entity));
				} else {
					resultHandler.handle(ServiceException.fail(500, res.cause().getMessage()));
				}
			});
		}
	}

	@Override
	public void remove(VideoStreamId id, Handler<AsyncResult<Boolean>> resultHandler)
	{
		if (vertx.isClustered()) {

		} else {
			if (localMap().containsKey(id.toJson())) {

				if (verticleLocalMap().containsKey(id.toJson())) {
					vertx.undeploy(verticleLocalMap().get(id.toJson()), res -> {
						if (res.succeeded()) {
							localMap().remove(id.toJson());
							resultHandler.handle(Future.succeededFuture(true));
						} else {
							resultHandler.handle(ServiceException.fail(500, res.cause().getMessage()));
						}
					});
				} else {
					localMap().remove(id.toJson());
					resultHandler.handle(Future.succeededFuture(true));
				}
			} else {
				resultHandler.handle(Future.succeededFuture(false));
			}
		}
	}

	@Override
	public void list(Handler<AsyncResult<List<VideoStreamEntity>>> resultHandler)
	{
		if (vertx.isClustered()) {
			vertx.sharedData().getClusterWideMap("video-streams", event -> {
				if (event.succeeded()) {
					resultHandler.handle(Future.succeededFuture((List) event.result()));
				} else {
					resultHandler.handle(ServiceException.fail(500, event.cause().getMessage()));
				}
			});
		} else {
			resultHandler.handle(Future.succeededFuture((List) localMap().values()));
		}
	}

	@Override
	public void single(VideoStreamId id, Handler<AsyncResult<VideoStreamEntity>> resultHandler)
	{
		if (vertx.isClustered()) {

		} else {
			Map<JsonObject, JsonObject> map = localMap();
			if (map.containsKey(id.toJson())) {
				resultHandler.handle(Future.succeededFuture(new VideoStreamEntity(map.get(id.toJson()))));
			} else {
				resultHandler.handle(Future.succeededFuture(null));
			}
		}
	}

	@Override
	public void clients(VideoStreamId id, Handler<AsyncResult<List<VideoStreamClientEntity>>> resultHandler)
	{
		if (vertx.isClustered()) {

		} else {
			if (localMap().containsKey(id.toJson())) {
				resultHandler.handle(Future.succeededFuture((List) clientLocalMap(id).values()));
			} else {
				resultHandler.handle(Future.succeededFuture(null));
			}
		}
	}

	@Override
	public void disconnectClient(VideoStreamId streamId, VideoStreamClientId clientId, Handler<AsyncResult<Boolean>> resultHandler)
	{
		if (vertx.isClustered()) {

		} else {
			if (clientLocalMap(streamId).containsKey(clientId.toJson())) {
				clientLocalMap(streamId).remove(clientId.toJson());
				resultHandler.handle(Future.succeededFuture(true));
			} else {
				resultHandler.handle(Future.succeededFuture(false));
			}
		}
	}
}
