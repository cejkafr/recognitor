package cz.klitea.recognitor.stream;

import io.vertx.core.Context;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.streams.ReadStream;

import java.io.IOException;

import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;

/**
 * Transforms frame grabber into ReadStream which can be turned into observable.
 * It is blocking since reading is done on same thread as where it is invoked, do not use in non-worker verticle.
 *
 * @author frantisek.cejka
 */
public class BlockingFrameMatrixReadStream implements ReadStream<FrameMatrix>
{
	final static Logger LOG = LoggerFactory.getLogger(BlockingFrameMatrixReadStream.class);
	/**
	 * @var For converting grabbed Frames to Matrix.
	 */
	final static OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();

	public static final int STATUS_PAUSED = 0, STATUS_ACTIVE = 1, STATUS_CLOSED = 2;

	private final Context context;
	private final FrameGrabber grabber;

	private Handler<Void> endHandler;
	private Handler<FrameMatrix> dataHandler;
	private Handler<Throwable> failureHandler;

	private int status;
	private boolean grabberRunning;

	private int optimalGrabInterval;
	private Frame frame;
	private FrameMatrix frameMatrix;
	private long index;
	private long startTs;
	private long frameStartTs;
	private long frameTimeSpent;
	private double frameDelay;

	public BlockingFrameMatrixReadStream(Vertx vertx, FrameGrabber grabber)
	{
		this.context = vertx.getOrCreateContext();
		this.grabber = grabber;
		this.status = STATUS_ACTIVE;
		this.grabberRunning = false;

		this.index = 0;
		this.frameMatrix = new FrameMatrix();
	}

	@Override
	public ReadStream<FrameMatrix> exceptionHandler(Handler handler)
	{
		this.failureHandler = handler;
		return this;
	}

	@Override
	public ReadStream endHandler(Handler handler)
	{
		this.endHandler = handler;
		return this;
	}

	@Override
	public ReadStream<FrameMatrix> handler(Handler<FrameMatrix> handler)
	{
		if (handler == null) {
			throw new UnsupportedOperationException("Handler cannot be null!");
		}

		this.dataHandler = handler;
		doRead();

		return this;
	}

	@Override
	public ReadStream<FrameMatrix> pause()
	{
		//if (isClosed()) {
		//	throw new IllegalStateException("Stream is already closed.");
		//}

		if (isActive()) {
			status = STATUS_PAUSED;
		}

		return this;
	}

	@Override
	public ReadStream<FrameMatrix> resume()
	{
		if (isClosed()) {
			throw new IllegalStateException("Stream is already closed.");
		}

		if (isPaused()) {
			status = STATUS_ACTIVE;
			doRead();
		}

		return this;
	}

	private void doRead()
	{
		try {
			if (!isActive()) {
				grabberStop();
				return;
			}

			if (!isGrabberRunning()) {
				grabberStart();
			}

			frameStartTs = System.currentTimeMillis();

			frame = grabber.grabFrame();

			if (frame == null) {
				status = STATUS_CLOSED;
				grabberStop();
				context.runOnContext(event -> {
					if (endHandler != null) {
						endHandler.handle(null);
					}
				});
				return;
			}

			frameMatrix.setNumber(frameMatrix.getNumber() + 1);
			frameMatrix.setTimestamp(System.currentTimeMillis());
			frameMatrix.setTime((System.currentTimeMillis() - startTs) / 1000.0);
			frameMatrix.setImageType(Java2DFrameConverter.getBufferedImageType(frame));
			frameMatrix.setData(converter.convert(frame));

			context.runOnContext(event -> {
				dataHandler.handle(frameMatrix);
				doRead();
			});

			frameTimeSpent = System.currentTimeMillis() - frameStartTs;
			frameDelay = optimalGrabInterval - frameTimeSpent;

			LOG.debug("Grabbed frame #{} in {}ms.", index, frameTimeSpent);
			if (frameDelay < 0) {
				LOG.warn("WARN: Grab took {}ms, slowed by {}ms.", frameTimeSpent, frameDelay * -1);
			}
		} catch (Exception ex) {
			context.runOnContext(event -> handleException(ex));
		}
	}

	private void grabberStart() throws IOException
	{
		if (!isGrabberRunning()) {
			grabberRunning = true;
			grabber.start();

			// TODO: does not take pause in mind
			if (startTs == 0) {
				startTs = System.currentTimeMillis();
			}

			optimalGrabInterval = (1000 / (int) grabber.getFrameRate()) + 1;
		}
	}

	private void grabberStop() throws IOException
	{
		if (isGrabberRunning()) {
			grabberRunning = false;
			grabber.stop();
		}
	}

	private void handleException(Throwable t)
	{
		if (failureHandler != null && t instanceof Exception) {
			failureHandler.handle(t);
		} else {
			LOG.error("Unhandled exception.", t);

		}
	}

	private boolean isGrabberRunning()
	{
		return grabberRunning;
	}

	private boolean isPaused()
	{
		return status == STATUS_PAUSED;
	}

	private boolean isClosed()
	{
		return status == STATUS_CLOSED;
	}

	private boolean isActive()
	{
		return status == STATUS_ACTIVE;
	}
}