package cz.klitea.recognitor.stream;

import io.vertx.core.Context;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.streams.WriteStream;
import org.bytedeco.javacv.FrameRecorder;

import java.io.IOException;

/**
 * Writes frames into frame recorder in vert.x write stream way.
 * This writer is blocking, so writing is done by same thread as it's invoked from. Queues doesn't matter.
 * Do not use in non-worker verticle.
 *
 * @author frantisek.cejka
 */
public class BlockingFrameMatrixWriteStream implements WriteStream<FrameMatrix>
{
	final static Logger LOG = LoggerFactory.getLogger(BlockingFrameMatrixWriteStream.class);

	public static final int STATUS_ACTIVE = 1, STATUS_CLOSED = 2;

	private final Context context;
	private final FrameRecorder recorder;

	private int status;

	private Handler<Void> drainHandler;
	private Handler<Throwable> failureHandler;

	public BlockingFrameMatrixWriteStream(Vertx vertx, FrameRecorder recorder)
	{
		this.context = vertx.getOrCreateContext();
		this.recorder = recorder;
		this.status = STATUS_ACTIVE;

		try {
			recorder.start();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public WriteStream<FrameMatrix> exceptionHandler(Handler<Throwable> handler)
	{
		this.failureHandler = handler;
		return this;
	}

	@Override
	public WriteStream<FrameMatrix> drainHandler(Handler<Void> handler)
	{
		this.drainHandler = handler;
		return this;
	}

	@Override
	public WriteStream<FrameMatrix> write(FrameMatrix frame)
	{
		if (status == STATUS_CLOSED) {
			context.runOnContext(event -> handleException(
					new IllegalStateException("Write stream is already closed.")));
			return this;
		}

		try {
			recorder.record(FrameMatrixReadStream.converter.convert(frame.getData()));
		} catch (IOException ex) {
			context.runOnContext(event -> handleException(ex));
		}
		return this;
	}

	@Override
	public void end()
	{
		try {
			status = STATUS_CLOSED;
			recorder.stop();
			recorder.close();
		} catch (IOException ex) {
			context.runOnContext(event -> handleException(ex));
		}
	}

	@Override
	public WriteStream<FrameMatrix> setWriteQueueMaxSize(int maxSize)
	{
		// probably not required in blocking writer
		return this;
	}

	@Override
	public boolean writeQueueFull()
	{
		// probably not required in blocking writer
		return false;
	}

	private void handleException(Throwable t)
	{
		if (failureHandler != null && t instanceof Exception) {
			failureHandler.handle(t);
		} else {
			LOG.error("Unhandled exception.", t);

		}
	}
}