package cz.klitea.recognitor.stream;

import org.bytedeco.javacpp.opencv_core;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class FrameMatrix
{
	private long timestamp;
	private double time;
	private long number;
	private int imageType;
	private opencv_core.Mat data;

	private BufferedImage image;

	public FrameMatrix()
	{
		data = new opencv_core.Mat();
	}

	public FrameMatrix(opencv_core.Mat data)
	{
		this.data = data;
	}

	public FrameMatrix(long timestamp, double time, long number,
					   int imageType, opencv_core.Mat data)
	{
		this.timestamp = timestamp;
		this.time = time;
		this.number = number;
		this.imageType = imageType;
		this.data = data;
	}

	protected void setTimestamp(long timestamp)
	{
		this.timestamp = timestamp;
	}

	protected void setTime(double time)
	{
		this.time = time;
	}

	protected void setNumber(long number)
	{
		this.number = number;
	}

	protected void setImageType(int imageType)
	{
		this.imageType = imageType;
	}

	protected void setData(opencv_core.Mat data)
	{
		this.data = data;
	}

	public long getTimestamp()
	{
		return timestamp;
	}

	public double getTime()
	{
		return time;
	}

	public long getNumber()
	{
		return number;
	}

	public opencv_core.Mat getData()
	{
		return data;
	}

	public BufferedImage toBufferedImage()
	{
		if (image == null) {
			image = new BufferedImage(getWidth(), getHeight(), imageType);
		}

//		Frame frame = H264Decoder.converter.convert(data);
//		Java2DFrameConverter.copy(frame, image);

		data.arrayData().get(((DataBufferByte) image.getRaster()
				.getDataBuffer()).getData());

		return image;
	}

	public int getWidth()
	{
		return data.arrayWidth();
	}

	public int getHeight()
	{
		return data.arrayHeight();
	}

	public int getDepth()
	{
		return data.arrayDepth();
	}

	public int getChannels()
	{
		return data.arrayChannels();
	}

	public int getImageType()
	{
		return imageType;
	}

	public void copyTo(FrameMatrix other)
	{
		other.timestamp = this.timestamp;
		other.time = this.time;
		other.number = this.number;
		other.imageType = this.imageType;
		this.data.copyTo(other.data);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof FrameMatrix)) return false;

		FrameMatrix that = (FrameMatrix) o;

		if (timestamp != that.timestamp) return false;
		if (Double.compare(that.time, time) != 0) return false;
		if (number != that.number) return false;
		if (imageType != that.imageType) return false;
		return data != null ? data.equals(that.data) : that.data == null;
	}
}
