package cz.klitea.recognitor.stream;

import cz.klitea.recognitor.entity.VideoStreamMetadata;
import cz.klitea.recognitor.utility.Functional;
import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.Context;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.streams.ReadStream;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;

import java.io.IOException;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Read stream designed to read frames from live camera.
 * Frames are converted to Matrix after they're read.
 * Supports auto-recovery when connection is lost or frames cannot be read.
 *
 * @author fcejka
 */
public class FrameMatrixReadStream implements ReadStream<FrameMatrix>
{
	final static Logger LOG = LoggerFactory.getLogger(FrameMatrixReadStream.class);

	/**
	 * @var For converting grabbed Frames to Matrix.
	 */
	public final static OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();

	public static final int
			/** @var Stream is active, but not available. */
			STATUS_UNAVAILABLE = -1,
	/**
	 * @var Stream paused, grabber should be too.
	 */
	STATUS_PAUSED = 0,
	/**
	 * @var Stream is active and available.
	 */
	STATUS_ACTIVE = 1,
	/**
	 * @var Stream is closed and it may never be resumed...NEVER!
	 */
	STATUS_CLOSED = 2;


	// Core data.

	/**
	 * @var Current context to deliver data.
	 */
	private final Context context;
	/**
	 * @var Thread on which we read frames.
	 */
	private final ExecutorService executor;
	/**
	 * @var Source of our data.
	 */
	private final FrameGrabber grabber;
	/**
	 * @var How long we should wait until we retry unavailable connection.
	 */
	private final int retryTimeout;


	// Handlers.

	/**
	 * @var Technically, live stream, never ends ...
	 */
	private Handler<Void> endHandler;
	/**
	 * @var Handler for grabbed frames.
	 */
	private Handler<FrameMatrix> dataHandler;
	/**
	 * @var When something nasty happens.
	 */
	private Handler<Throwable> failureHandler;


	// Current states.

	/**
	 * @var Current status.
	 */
	private int status;
	/**
	 * @var Equals true when FrameGrabber is running, it doesn't provide info about its state.
	 */
	private boolean running;
	/**
	 * @var Time since stream start.
	 */
	private Instant startedAt;
	/**
	 * @var Time since stream is unavailable.
	 */
	private Instant unavailableSince;


	// Variables used in reading loop, so they're predefined.

	/**
	 * @var Metadata about current resource stream.
	 */
	private VideoStreamMetadata metadata;
	/**
	 * @var Grabbed frame.
	 */
	private Frame frame;
	/**
	 * @var Pre-initialized object, using same object for each frame.
	 */
	private FrameMatrix frameMatrix;
	/**
	 * @var Start grabber timestamp.
	 */
	private long startTs;
	/**
	 * @var Grab start timestamp.
	 */
	private long grabStartTs;
	/**
	 * @var Time spend grabbing a single frame.
	 */
	private long grabTime;
	/**
	 * @var Optimal time, to grab single frame based on current framerate.
	 */
	private int optimalGrabbingTime;
	/**
	 * @var Real time spent grabbing single frame.
	 */
	private double realGrabbingTime;

	private boolean forceFramerate;


	public FrameMatrixReadStream(final Vertx vertx, final ExecutorService executor, final FrameGrabber grabber)
	{
		this.context = vertx.getOrCreateContext();
		this.executor = executor;
		this.grabber = grabber;
		this.retryTimeout = 5000;

		this.status = STATUS_PAUSED;
		this.running = false;

		this.frameMatrix = new FrameMatrix();

		this.forceFramerate = true;
	}

	@Override
	public ReadStream handler(@Nullable Handler<FrameMatrix> handler)
	{
		if (handler == null)
			throw new UnsupportedOperationException("Handler cannot be null!");

		this.dataHandler = handler;
		status = STATUS_ACTIVE;
		doRead();

		return this;
	}

	@Override
	public ReadStream endHandler(@Nullable Handler endHandler)
	{
		this.endHandler = endHandler;
		return this;
	}

	@Override
	public ReadStream exceptionHandler(Handler handler)
	{
		this.failureHandler = handler;
		return this;
	}

	@Override
	public ReadStream pause()
	{
		if (status == STATUS_ACTIVE || status == STATUS_UNAVAILABLE) {
			status = STATUS_PAUSED;
		}

		return this;
	}

	@Override
	public ReadStream resume()
	{
		switch (status) {
			case STATUS_CLOSED:
				throw new IllegalStateException();
			case STATUS_PAUSED:
				status = STATUS_ACTIVE;
				doRead();
		}

		return this;
	}

	/**
	 * Closes stream and underlying grabber.
	 *
	 * @return
	 */
	public ReadStream close()
	{
		try {
			// We have to try shutdown all running grabbing threads, we may encounter SIGSEGV.
			try {
				executor.shutdownNow();
				executor.awaitTermination(500, TimeUnit.MILLISECONDS);
			} catch (InterruptedException ex) {
				LOG.warn("Failed to terminate all tasks, you may encounter a SIGSEGV.");
			}

			stopGrabber();
			grabber.close();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		return this;
	}

	/**
	 * Return true if frame grabber is running.
	 *
	 * @return
	 */
	public boolean isRunning()
	{
		return running;
	}

	/**
	 * Return true if stream is active but unavailable.
	 *
	 * @return
	 */
	public boolean isUnavailable()
	{
		return status == STATUS_UNAVAILABLE;
	}

	/**
	 * Return true if stream is paused.
	 *
	 * @return
	 */
	public boolean isPaused()
	{
		return status == STATUS_PAUSED;
	}

	/**
	 * Return true if stream is closed.
	 *
	 * @return
	 */
	public boolean isClosed()
	{
		return status == STATUS_CLOSED;
	}

	/**
	 * Return true if stream is active and available.
	 *
	 * @return
	 */
	public boolean isActive()
	{
		return status == STATUS_ACTIVE;
	}

	/**
	 * Return last frame index.
	 *
	 * @return
	 */
	public long getLastFrameIndex()
	{
		return frameMatrix.getNumber();
	}

	/**
	 * Returns metadata about consumed stream.
	 *
	 * @return
	 */
	public VideoStreamMetadata getMetadata()
	{
		return metadata;
	}

	/**
	 * Time when stream started.
	 *
	 * @return
	 */
	public Instant getStartedAt()
	{
		return startedAt;
	}

	/**
	 * Time since stream is unavailable.
	 *
	 * @return
	 */
	public Instant getUnavailableSince()
	{
		return unavailableSince;
	}

	/**
	 * Handles actual reading from FrameGrabber.
	 * @TODO: Too long, split into multiple functions?
	 */
	private void doRead()
	{
		//LOG.info("CURRENT STATUS {}, RUNNING {}", status, running);

		if (executor.isShutdown()) {
			return;
		}

		// Handled in different thread, so we're not blocking main thread.
		executor.execute(() -> {

			// Stream is active and available, let's grab some frames and rock.
			if (status == STATUS_ACTIVE) {

				//LOG.info("ACTIVE, GRABBING FRAME.");

				try {

					// Start grabber if it's not started yet.
					startGrabber();

					grabStartTs = System.currentTimeMillis();

					frame = grabber.grabFrame();

					// We've got the frame, let's convert it and send response.
					if (frame != null) {

						frameMatrix.setNumber(frameMatrix.getNumber() + 1);
						frameMatrix.setTimestamp(System.currentTimeMillis());
						frameMatrix.setTime((System.currentTimeMillis() - startTs) / 1000.0);
						frameMatrix.setImageType(Java2DFrameConverter.getBufferedImageType(frame));
						frameMatrix.setData(converter.convert(frame));

						context.runOnContext(event -> {
							dataHandler.handle(frameMatrix);
							doRead();
						});

						// Some metrics for debugging.
						grabTime = System.currentTimeMillis() - grabStartTs;
						realGrabbingTime = optimalGrabbingTime - grabTime;

						if (grabTime < optimalGrabbingTime && forceFramerate) {
							try {
								Thread.sleep(optimalGrabbingTime - grabTime);
							} catch (InterruptedException ex)
							{}
						}

						LOG.info("Grabbed frame #{} in {}ms.", frameMatrix.getNumber(), grabTime);
						if (realGrabbingTime < 0) {
							//LOG.warn("WARN: Grab took {}ms, slowed by {}ms.", grabTime, realGrabbingTime * -1);
						}

						// We've failed to grab a frame, stream is probably unavailable
					} else {

						handleUnavailable(null);

					}


					// We've actually failed while connecting to the resource
				} catch (IOException ex) {

					handleUnavailable(ex);

				}

				// Stream is unavailable, let's try to restart connection.
			} else if (status == STATUS_UNAVAILABLE) {

				restartGrabber();

				// Stream is probably paused or closed, either way, we have to close grabber if it's not.
			} else {

				LOG.info("Pausing stream and stopping grabber.");
				stopGrabber();

			}

		});
	}

	/**
	 * Start grabber.
	 */
	private void startGrabber() throws IOException
	{
		if (!isRunning()) {
			//LOG.info("REAL STARTING GRABBER.");

			startedAt = Instant.now();

			grabber.start();

			running = true;
			optimalGrabbingTime = (1000 / (int) grabber.getFrameRate()) + 1;

			metadata = Functional.videoMetadata(grabber);

			if (!isUnavailable())
				startTs = System.currentTimeMillis();
		}
	}

	/**
	 * Stop grabber.
	 */
	private void stopGrabber()
	{
		if (isRunning()) {
			//LOG.info("REAL STOPPING GRABBER.");

			try {
				grabber.stop();
				running = false;

				metadata = null;

				if (!isUnavailable())
					startedAt = null;
			} catch (IOException ex) {
				LOG.error(ex);
			}
		}
	}

	/**
	 * Restart grabber.
	 */
	private void restartGrabber()
	{
		LOG.warn("Resource NOT AVAILABLE for {}s; Retry every {}ms.",
				unavailableSince.getEpochSecond() - startedAt.getEpochSecond(), retryTimeout);

		try {
			startGrabber();
			if (metadata != null) {
				LOG.warn("Resource RECONNECTED after {}s.",
						unavailableSince.getEpochSecond() - startedAt.getEpochSecond());
				status = STATUS_ACTIVE;
				unavailableSince = null;
			} else {
				stopGrabber();
			}
		} catch (IOException ex) {
			stopGrabber();
		}

		try {
			Thread.sleep(retryTimeout);
		} catch (InterruptedException ex1) {
			// Nothing to do
		}

		context.runOnContext(event -> doRead());
	}

	/**
	 * Handle unavailable stream, execute retry if stream cannot be read from, invokes exception handler when theres other error.
	 *
	 * @param ex
	 */
	private void handleUnavailable(Throwable ex)
	{
		if (ex != null) {
			if (ex instanceof FrameGrabber.Exception) {
				if (!ex.getMessage().startsWith("avformat_open_input() error -113") && !ex.getMessage().startsWith("avformat_open_input() error -110")) {
					if (failureHandler != null && ex instanceof Exception) {
						failureHandler.handle(ex);
					} else {
						LOG.error("Unhandled exception.", ex);
					}

					return;
				}
			}
		}

		LOG.warn("Resource is unavailable!", retryTimeout);

		status = STATUS_UNAVAILABLE;
		unavailableSince = Instant.now();
		stopGrabber();

		context.runOnContext(event -> doRead());
	}
}
