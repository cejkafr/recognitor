package cz.klitea.recognitor.utility;

import cz.klitea.recognitor.entity.VideoStreamMetadata;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.CompositeFuture;
import io.vertx.rxjava.core.Future;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacv.FrameGrabber;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Helper class.
 * <p>
 * TODO: move video functions to separate helper class.
 *
 * @author frantisek.cejka
 */
public abstract class Functional
{
	final static Logger LOG = LoggerFactory.getLogger(Functional.class);

	public static Future<List> sequenceFuture(List<Future> futures)
	{
		return CompositeFuture.all(futures)
				.map(v -> futures.stream()
						.map(Future::result)
						.collect(Collectors.toList())
				);
	}

	/*public static String enhanceByAuth(String resourceUrl, LiveResourceAuthEntity auth)
	{
		if (auth == null)  return resourceUrl;
		int index = resourceUrl.indexOf("://") + 3;
		return resourceUrl.substring(0, index) +
				auth.getUsername() + ":" +
				auth.getPassword() + "@" +
				resourceUrl.substring(index);
	}*/

	/*public static VideoStreamMetadata.VideoStreamMetadataResponse readMetadata(final String url)
	{
		ResourceConfig.Input config = new ResourceConfig.Input("id", url,
				null, "h264", 0, 0, 0, null);

		try {
			H264Decoder decoder = new H264Decoder(config);
			decoder.start();
			VideoStreamMetadata metadata = Functional.videoMetadata(decoder.getGrabber());
			decoder.stop();
			return new VideoStreamMetadata.VideoStreamMetadataResponse(metadata, "OK");
		} catch (IOException ex) {

			String status = "NOT_AVAILABLE";
			if (ex.getMessage().startsWith("avformat_open_input() error -825242872")) {
				LOG.info("Unable to connect to resource, INVALID_CREDENTIALS.");
				status = "INVALID_CREDENTIALS";
			}

			return new VideoStreamMetadata.VideoStreamMetadataResponse(null, status);
		}
	}*/

	public static VideoStreamMetadata videoMetadata(FrameGrabber grabber)
	{
		try {
			boolean started = false;
			if (grabber.getImageWidth() == 0) {
				LOG.info("Cannot read image width, grabber is probably stopped, starting.");

				started = true;
				grabber.start();
				grabber.grabFrame();
			}

			VideoStreamMetadata videoDesc = new VideoStreamMetadata()
					.setImageWidth(grabber.getImageWidth())
					.setImageHeight(grabber.getImageHeight())
					.setFrameRate((int) grabber.getFrameRate())
					.setVideoBitrate(grabber.getVideoBitrate())
					.setLengthInFrames(grabber.getLengthInFrames())
					.setLengthInSec(grabber.getLengthInTime() / 1000000)
					.setCodec(videoCodecToString(grabber.getVideoCodec()))
					.setFormat(grabber.getFormat())
					.setPixelFormat(grabber.getPixelFormat());

			if (started) {
				grabber.stop();
			}

			return videoDesc;
		} catch (IOException ex) {

			return null;
		}
	}

	protected static String videoCodecToString(int videoCodecInt)
	{
		switch (videoCodecInt) {
			case avcodec.AV_CODEC_ID_H264:
				return "H.264";
			case avcodec.AV_CODEC_ID_MJPEG:
				return "MJPEG";
			default:
				return Integer.toString(videoCodecInt);
		}
	}

	public static Double pingHost(String str)
	{
		return pingHost(str, 5);
	}

	public static Double pingHost(String str, int retry)
	{
		return pingHost(str, retry, 2000);
	}

	public static Double pingHost(String str, int retry, int timeout)
	{
		double[] responses = new double[retry];
		long startTs;

		try {
			// frantisek.cejka: URL does not support RTSP protocol.
			if (str.startsWith("rtsp://"))
				str = "http" + str.substring(4);

			// These protocols are not pingable.
			if (str.startsWith("devId://"))
				return null;

			URL url = new URL(str);
			InetAddress inet = InetAddress.getByName(url.getHost());

			for (int i = 0; i < retry; i++) {
				startTs = System.nanoTime();
				if (inet.isReachable(timeout)) {
					responses[i] = ((System.nanoTime() - startTs));
				} else {
					responses[i] = timeout;
				}
			}
		} catch (IOException ex) {
			LOG.warn(ex);
			return null;
		}

		double avg = Arrays.stream(responses).average().getAsDouble() / 1000000.0;
		return Math.ceil(avg * 1000.0) / 1000.0;
	}
}